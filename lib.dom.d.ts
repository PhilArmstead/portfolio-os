import type { File, ProcessOpenParameter } from './types'

declare global {
	interface DocumentEventMap {
		'process:open': CustomEvent<ProcessOpenParameter>;
		'file:execute': CustomEvent<File>;
	}
}
