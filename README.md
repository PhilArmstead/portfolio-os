# Building

Upon building, the contents of `src/index.html` will be minified and the lazy script-loader will be injected in the
footer.

The following will need to be done to manually finalise the `dist/index.html` file.

1. Copy the `index-[hash].js` hash from the header
2. Delete the import of `index-[hash].js` from the header
3. Paste the hash in to the lazy-load script in the footer
4. Find the hash of the Windows CSS theme
5. Paste the hash in to the lazy-load theme link
