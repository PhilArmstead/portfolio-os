/// <reference types="vitest" />
import { createHtmlPlugin } from 'vite-plugin-html'
import { defineConfig } from 'vite'
import { viteExternalsPlugin } from 'vite-plugin-externals'
import vue from '@vitejs/plugin-vue'

const testFiles = ['**/*.spec.ts']

const PRODUCTION_PLUGINS = process.env.NODE_ENV === 'production' ? [
	viteExternalsPlugin({ vue: 'Vue' }),
	createHtmlPlugin({
		minify: true,
		inject: {
			data: {
				jsFallback: '<script>' +
					'const main = document.createElement("script");' +
					'main.setAttribute("type", "module");' +
					'main.setAttribute("crossorigin", "true");' +
					'main.setAttribute("src", "/assets/index-5a71d575.js");' +
					'main.addEventListener("error", () => {' +
					'const noscript = document.querySelector("noscript");' +
					'noscript.outerHTML = noscript.innerHTML' +
					'});' +
					'document.body.insertAdjacentElement("beforeend", main)' +
					'</script>'
				,
			},
		},
	}),
] : [
	createHtmlPlugin({
		minify: false,
		inject: { data: { jsFallback: '' } },
	}),
]

export default defineConfig({
	base: '',
	root: './src',
	publicDir: '../public',
	build: {
		outDir: '../dist',
		rollupOptions: {
			external: ['vue'],
			output: {
				compact: true,
			},
		},
	},
	plugins: [
		vue(),
		...PRODUCTION_PLUGINS,
	],
	test: {
		include: testFiles,
		environment: 'happy-dom',
		coverage: {
			enabled: true,
			provider: 'v8',
			exclude: testFiles.concat('**/*.po.ts', 'main.ts', 'data/*', 'enums/*', '**/index.ts'),
			branches: 100,
			statements: 100,
		},
	},
})
