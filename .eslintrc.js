module.exports = {
	env: {
		es2022: true,
		node: true,
	},
	extends: [
		'eslint:recommended',
		'plugin:import/errors',
	],
	overrides: [
		{
			files: ['**/*.{ts,vue}'],
			parser: '@typescript-eslint/parser',
			plugins: ['@typescript-eslint'],
			extends: ['plugin:import/typescript', 'plugin:@typescript-eslint/recommended'],
			rules: {
				'no-unused-vars': 'off',
				'@typescript-eslint/no-unused-vars': ['error', { argsIgnorePattern: '_' }],
				'@typescript-eslint/consistent-type-definitions': ['error'],
				'@typescript-eslint/consistent-type-imports': ['error'],
				'@typescript-eslint/explicit-function-return-type': ['error'],
				'@typescript-eslint/consistent-indexed-object-style': ['error'],
				'@typescript-eslint/member-delimiter-style': ['error'],
				'@typescript-eslint/method-signature-style': ['error'],
				'@typescript-eslint/prefer-for-of': ['error'],
				'@typescript-eslint/type-annotation-spacing': ['error'],
				'@typescript-eslint/explicit-module-boundary-types': ['error'],
			},
		},
		{
			files: ['**/*.json'],
			extends: ['plugin:json/recommended'],
			rules: {
				'comma-dangle': [
					'warn', {
						arrays: 'never',
						exports: 'never',
						functions: 'never',
						imports: 'never',
						objects: 'never',
					},
				],
			},
		},
		{
			files: ['**/*.vue'],
			env: { browser: true },
			extends: ['plugin:vue/vue3-recommended', '@vue/eslint-config-typescript'],
			parserOptions: {
				tsconfigRootDir: __dirname,
				project: ['./tsconfig.json'],
			},
			rules: {
				'vue/html-closing-bracket-newline': 'warn',
				'vue/max-attributes-per-line': [
					'error', {
						singleline: 3,
						multiline: 1,
					},
				],
				'vue/html-quotes': ['warn', 'single', { avoidEscape: true }],
				'vue/html-indent': [
					'warn', 'tab', {
						'attribute': 1,
						'baseIndent': 1,
						'closeBracket': 0,
						'alignAttributesVertically': true,
						'ignores': [],
					},
				],
				'vue/script-indent': [
					'error', 'tab', { baseIndent: 1, switchCase: 1 },
				],
				'indent': 'off',
				'vue/no-textarea-mustache': 'warn',
			},
		},
	],
	ignorePatterns: ['.idea', 'dist', 'coverage', 'node_modules', 'package-lock.json'],
	rules: {
		'no-console': 'error',
		'no-debugger': 'error',
		'max-len': ['warn', { code: 130, tabWidth: 2 }],
		'array-bracket-spacing': 'off',
		'object-curly-spacing': ['error', 'always'],
		'semi': ['warn', 'never'],
		'space-before-function-paren': ['warn', 'always'],
		'indent': ['error', 'tab', { SwitchCase: 1 }],
		'no-tabs': ['off', { allowIndentationTabs: true }],
		'comma-dangle': [
			'error', {
				arrays: 'always-multiline',
				exports: 'always-multiline',
				functions: 'never',
				imports: 'always-multiline',
				objects: 'always-multiline',
			},
		],
		'quotes': ['error', 'single'],
		'quote-props': ['error', 'consistent'],
		'no-var': 'error',
		'sort-imports': [
			'error', {
				'ignoreCase': true,
				'allowSeparatedGroups': true,
			},
		],
	},
}
