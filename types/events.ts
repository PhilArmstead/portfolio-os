import type { Component } from 'vue'
import type { File } from './files'

export interface ProcessOpenParameter {
	component: Component;
	file: File;
}
