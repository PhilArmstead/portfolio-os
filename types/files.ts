import type { Component } from 'vue'

import type { FileType } from '../src/enums/files'
import type { Size } from './common'

export interface Process {
	component: Component;
	file: File;
	id: number;
	isMinimised?: boolean;
	zIndex: number;
}

export interface File {
	data: ProcessData;
	/** Path to image file */
	icon?: string;
	/** Display name of file */
	meta: ProcessMeta;
	name: string;
	/** Determines the opened Process and fallback icon */
	type: FileType;
}

export interface ProcessData {
	content?: string;
	dimensions?: Size;
	file?: string; // Is this right?
	files?: File[];
	readOnly?: boolean;
}

export interface ProcessMeta {
	cannotMaximise?: boolean;
	cannotMinimise?: boolean;
	cannotResize?: boolean;
	defaultSize?: Size;
	readonly?: boolean;
	title?: string;
}
