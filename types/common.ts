export interface Size {
	height: number;
	width: number;
}

export interface Point {
	x: number;
	y: number;
}
