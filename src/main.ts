import { createApp } from 'vue'

import App from './components/App/App.vue'
import initialise from './components/App/initialise'

initialise()

createApp(App).mount('#app')
