import { operatingSystem } from '../../store/client'

export default (): void => {
	document.firstElementChild?.classList.add(`theme--${operatingSystem}`)

	if (operatingSystem === 'win') {
		import('../../scss/themes/windows/index.scss')
	} else {
		import('../../scss/themes/macos/index.scss')
	}
}
