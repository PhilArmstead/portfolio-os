import { beforeAll, describe, expect, it, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import App from './App.vue'
import PageObject from './App.vue.spec.po'

const mockBlurActiveProcess = vi.fn()
const mockMinimiseToggle = vi.fn()
const mockRemoveProcess = vi.fn()
const mockSetProcessFocus = vi.fn()

vi.mock('../../store/processes', () => ({
	blurActiveProcess: (): void => mockBlurActiveProcess(),
	minimiseToggle: (id: number): void => mockMinimiseToggle(id),
	removeProcess: (index: number): void => mockRemoveProcess(index),
	setProcessFocus: (clickedId: number): void => mockSetProcessFocus(clickedId),
}))

describe('App', () => {
	describe('When mounting component', () => {
		let pageObject: PageObject

		beforeAll(async () => {
			pageObject = new PageObject(shallowMount(App))
		})

		describe('And <the-desktop> emits "click" event', () => {
			beforeAll(() => {
				mockBlurActiveProcess.mockReset()
				pageObject.component.desktop().trigger('click')
			})

			it('Should call `blurActiveProcess`', () => {
				expect(mockBlurActiveProcess).toHaveBeenCalledTimes(1)
			})
		})

		describe('And <the-desktop> emits "process:close" event', () => {
			beforeAll(() => {
				mockRemoveProcess.mockReset()
				pageObject.component.desktop().trigger('process:close', { value: 10 })
			})

			it('Should call `removeProcess`', () => {
				expect(mockRemoveProcess).toHaveBeenCalledTimes(1)
				expect(mockRemoveProcess).toHaveBeenCalledWith(expect.objectContaining({ value: 10 }))
			})
		})

		describe('And <the-desktop> emits "process:focus" event', () => {
			beforeAll(() => {
				mockSetProcessFocus.mockReset()
				pageObject.component.desktop().trigger('process:focus', { value: 5 })
			})

			it('Should call `setProcessFocus`', () => {
				expect(mockSetProcessFocus).toHaveBeenCalledTimes(1)
				expect(mockSetProcessFocus).toHaveBeenCalledWith(expect.objectContaining({ value: 5 }))
			})
		})

		describe('And <the-desktop> emits "process:minimise" event', () => {
			beforeAll(() => {
				mockBlurActiveProcess.mockReset()
				pageObject.component.desktop().trigger('process:minimise')
			})

			it('Should call `blurActiveProcess`', () => {
				expect(mockBlurActiveProcess).toHaveBeenCalledTimes(1)
			})
		})

		describe('And <the-taskbar> emits "click" event', () => {
			beforeAll(() => {
				mockBlurActiveProcess.mockReset()
				pageObject.component.taskbar().trigger('click')
			})

			it('Should call `blurActiveProcess`', () => {
				expect(mockBlurActiveProcess).toHaveBeenCalledTimes(1)
			})
		})

		describe('And <the-taskbar> emits "taskbar:application-click" event', () => {
			beforeAll(() => {
				mockMinimiseToggle.mockReset()
				pageObject.component.taskbar().trigger('taskbar:application-click', { value: 14 })
			})

			it('Should call `minimiseToggle`', () => {
				expect(mockMinimiseToggle).toHaveBeenCalledTimes(1)
				expect(mockMinimiseToggle).toHaveBeenCalledWith(expect.objectContaining({ value: 14 }))
			})
		})
	})
})
