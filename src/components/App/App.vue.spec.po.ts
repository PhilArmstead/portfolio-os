import type { VueWrapper } from '@vue/test-utils'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'desktop' | 'taskbar', () => VueWrapper> {
		return {
			desktop: () => this.wrapper.findComponent('.app__desktop') as VueWrapper,
			taskbar: () => this.wrapper.findComponent('.app__taskbar') as VueWrapper,
		}
	}
}
