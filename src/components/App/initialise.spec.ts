import { beforeAll, describe, expect, it, vi } from 'vitest'

import * as clientStore from '../../store/client'
import initialise from './initialise'

const mockOperatingSystem = vi.spyOn(clientStore, 'operatingSystem', 'get')
const mockWindowsStylesImport = vi.fn()
const mockMacStylesImport = vi.fn()

vi.mock('../../scss/themes/windows/index.scss', () => ({
	default: mockWindowsStylesImport(),
}))
vi.mock('../../scss/themes/macos/index.scss', () => ({
	default: mockMacStylesImport(),
}))

describe('When calling `initialise`', () => {
	describe('And `operatingSystem` is "win"', () => {
		beforeAll(() => {
			mockWindowsStylesImport.mockReset()
			mockMacStylesImport.mockReset()
			mockOperatingSystem.mockReturnValue('win')
			initialise()
		})

		it('Should add class to HTML element', () => {
			expect(document.firstElementChild?.classList.contains('theme--win')).toBe(true)
		})

		it('Should import Windows themes', () => {
			expect(mockWindowsStylesImport).toHaveBeenCalledTimes(1)
		})

		it('Should not import MacOS themes', () => {
			expect(mockMacStylesImport).not.toHaveBeenCalled()
		})
	})

	describe('And `operatingSystem` is something else', () => {
		beforeAll(() => {
			mockWindowsStylesImport.mockReset()
			mockMacStylesImport.mockReset()
			mockOperatingSystem.mockReturnValue('mac')
			initialise()
		})

		it('Should add class to HTML element', () => {
			expect(document.firstElementChild?.classList.contains('theme--mac')).toBe(true)
		})

		it('Should not import Windows themes', () => {
			expect(mockWindowsStylesImport).not.toHaveBeenCalled()
		})

		it('Should import MacOS themes', () => {
			expect(mockMacStylesImport).toHaveBeenCalledTimes(1)
		})
	})
})
