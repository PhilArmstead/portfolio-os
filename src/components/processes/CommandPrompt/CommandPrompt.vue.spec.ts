import { beforeAll, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'

import CommandPrompt from './CommandPrompt.vue'
import type { File } from '../../../../types'
import { FileType } from '../../../enums/files'
import PageObject from './CommandPrompt.vue.spec.po'

describe('CommandPrompt', () => {
	describe('When mounting component', () => {
		const file: File = {
			data: { content: '<h1>Hello, world!</h1>' },
			meta: {},
			name: 'hello-world',
			type: FileType.SHELL,
		}
		let pageObject: PageObject

		beforeAll(async () => {
			pageObject = new PageObject(mount(CommandPrompt, { props: { file } }))
		})

		it('Should pass `file` prop to <base-process />', () => {
			expect(pageObject.component.baseProcess().props()).toEqual({ file })
		})

		it('Should display content', () => {
			expect(pageObject.element.content().html()).toContain(file.data.content)
		})
	})
})
