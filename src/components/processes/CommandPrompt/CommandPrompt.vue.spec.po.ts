import type { DOMWrapper, VueWrapper } from '@vue/test-utils'

import BaseProcess from '../BaseProcess/BaseProcess.vue'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'baseProcess', () => VueWrapper> {
		return {
			baseProcess: () => this.wrapper.findComponent(BaseProcess) as VueWrapper,
		}
	}

	get element (): Record<'content', () => DOMWrapper<Element>> {
		return {
			content: () => this.wrapper.find('.process--shell code'),
		}
	}
}
