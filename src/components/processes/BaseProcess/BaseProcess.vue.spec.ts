import { afterAll, beforeAll, describe, expect, it, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import BaseProcess from './BaseProcess.vue'
import type { ComponentPublicInstance } from 'vue'
import type { File } from '../../../../types'
import { FileType } from '../../../enums/files'
import { nextTick } from 'vue'
import PageObject from './BaseProcess.vue.spec.po'

const mockAddEventListener = vi.spyOn(document.body, 'addEventListener')
const mockRemoveEventListener = vi.spyOn(document.body, 'removeEventListener')

const dimensions = {
	container: { offsetHeight: 600, offsetWidth: 1200 },
	process: { offsetHeight: 300, offsetWidth: 400 },
}

Object.defineProperty(HTMLDivElement.prototype, 'offsetWidth', {
	get () {
		return dimensions[this.classList.contains('process') ? 'process' : 'container'].offsetWidth
	},
	set () {
		return 0
	},
	enumerable: true,
	configurable: true,
})
Object.defineProperty(HTMLDivElement.prototype, 'offsetHeight', {
	get () {
		return dimensions[this.classList.contains('process') ? 'process' : 'container'].offsetHeight
	},
	set () {
		return 0
	},
	enumerable: true,
	configurable: true,
})

const mockObserver = vi.fn()
const resizeObserverMock = vi.spyOn(window, 'ResizeObserver')
resizeObserverMock.mockReturnValue({
	disconnect: vi.fn(),
	observe: (element: HTMLElement) => mockObserver(element),
	unobserve: vi.fn(),
})

const setup = (file: File): PageObject => new PageObject(shallowMount(BaseProcess, {
	props: { file },
	slots: {
		default: '<h1>Hello, world!</h1>',
	},
}))

describe('When mounting component', () => {
	let pageObject: PageObject
	const file: File = {
		data: {},
		meta: { title: 'My smashing title' },
		name: 'My File',
		type: FileType.TEXT,
	}

	beforeAll(() => {
		pageObject = setup(file)
	})

	afterAll(() => {
		resizeObserverMock.mockClear()
	})

	it('Should add "can-resize" class', () => {
		expect(pageObject.element.process().classes('process--can-resize')).toBe(true)
	})

	it('Should not add "--maximised" class', () => {
		expect(pageObject.element.root().classes('process-container--maximised')).toBeFalsy()
	})

	it('Should not add "--minimised" class', () => {
		expect(pageObject.element.root().classes('process-container--minimised')).toBe(false)
	})

	it('Should pass callback function to new ResizeObserver', () => {
		expect(ResizeObserver).toHaveBeenCalledWith(expect.any(Function))
	})

	it('Should call ResizeObserver.observe on process element', () => {
		expect(mockObserver).toHaveBeenCalledWith(pageObject.element.process().element)
	})

	it('Should set the element\'s size and position', () => {
		const style = pageObject.element.process().attributes('style')
		const width = dimensions.process.offsetWidth
		const height = dimensions.process.offsetHeight
		const left = dimensions.container.offsetWidth / 2 - width / 2
		const top = dimensions.container.offsetHeight / 2 - height / 2
		expect(style).toBe(`transform: translate(${left}px, ${top}px); width: ${width}px; height: ${height}px;`)
	})

	it('Should print `title` prop to element', () => {
		expect(pageObject.element.title().text()).toBe(file.meta.title)
	})

	it('Should show maximise button', () => {
		expect(pageObject.element.maximise().exists()).toBe(true)
	})

	it('Should show minimise button', () => {
		expect(pageObject.element.minimise().exists()).toBe(true)
	})

	it('Should embed slot content', () => {
		expect(pageObject.element.content().html()).toContain('Hello, world!')
	})

	it('Should not display toolbar icon', () => {
		expect(pageObject.element.icon().exists()).toBe(false)
	})

	it('Should not emit "process:focus" event', () => {
		expect(pageObject.emitted('process:focus')).toBeFalsy()
	})

	it('Should not emit "process:close" event', () => {
		expect(pageObject.emitted('process:close')).toBeFalsy()
	})

	it('Should not emit "process:minimise" event', () => {
		expect(pageObject.emitted('process:minimise')).toBeFalsy()
	})

	describe('And ResizeObserver callback fires', () => {
		const mockWidth = 444
		const mockHeight = 111

		describe.each([
			[false, false, false, false],
			[false, false, true, false],
			[false, true, false, false],
			[false, true, true, false],
			[true, false, false, true],
			[true, false, true, false],
			[true, true, false, false],
			[true, true, true, false],
		])('And `hasInitialised` is %s', (hasInitialised, isMaximised, isMinimised, shouldResize) => {
			describe(`And \`isMaximised\` is ${isMaximised}`, () => {
				describe(`And \`isMinimised\` is ${isMinimised}`, () => {
					beforeAll(async () => {
						pageObject.setRef('hasInitialised', hasInitialised)
						pageObject.setRef('isMaximised', isMaximised)
						pageObject.setRef('isMinimised', isMinimised)
						pageObject.setRef('processHeight', -1)
						pageObject.setRef('processWidth', -1)

						// eslint-disable-next-line @typescript-eslint/ban-ts-comment
						// @ts-ignore
						resizeObserverMock.mock.calls.slice(-1)[0][0]([{
							borderBoxSize: [{ inlineSize: mockWidth, blockSize: mockHeight }],
						}])
					})

					it(`Should ${!shouldResize ? 'NOT ' : ''}update dimensions`, () => {
						expect(pageObject.getRef('processWidth') === mockWidth).toBe(shouldResize)
						expect(pageObject.getRef('processHeight') === mockHeight).toBe(shouldResize)
					})
				})
			})
		})
	})

	describe('And clicking minimise button', () => {
		beforeAll(async () => await pageObject.element.minimise().trigger('click'))

		it('Should add "--minimised" class', () => {
			expect(pageObject.element.root().classes('process-container--minimised')).toBe(true)
		})

		it('Should emit "process:minimise" event', () => {
			expect(pageObject.emitted('process:minimise')).toHaveLength(1)
		})
	})

	describe('And clicking close button', () => {
		it('Should emit "process:close" event', async () => {
			await pageObject.element.close().trigger('click')
			expect(pageObject.emitted('process:close')).toHaveLength(1)
		})
	})

	describe('And "process:set-is-minimised" event triggers on root element', () => {
		describe.each([[true], [false]])('And `event.detail` is %s', (isMinimised) => {
			it(`Should ${!isMinimised ? 'NOT' : ''}add "--minimised" class`, async () => {
				await pageObject.element.root().trigger('process:set-is-minimised', { detail: isMinimised })
				expect(pageObject.element.root().classes('process-container--minimised')).toBe(isMinimised)
			})
		})
	})

	describe('And "mousedown" event triggers on process element', () => {
		it('Should emit "process:focus" event', async () => {
			await pageObject.element.process().trigger('mousedown')
			expect(pageObject.emitted('process:focus')).toHaveLength(1)
		})
	})

	describe.each([
		['clicking maximise button', 'maximise', 'click'],
		['double-clicking title element', 'title', 'dblclick'],
	])('And %s', (_, elementName, eventName) => {
		let emittedCount: number

		beforeAll(async () => {
			pageObject.setRef('isMaximised', false)
			emittedCount = pageObject.emitted('process:focus')?.length || 0
			await pageObject.element[elementName as 'maximise' | 'title']().trigger(eventName)
		})

		it('Should add "maximised" class', () => {
			expect(pageObject.element.root().classes('process-container--maximised')).toBe(true)
		})

		it('Should emit "process:focus" event', () => {
			expect(pageObject.emitted('process:focus')).toHaveLength(emittedCount + 1)
		})

		describe('And repeating action', () => {
			let emittedCount: number

			beforeAll(async () => {
				emittedCount = pageObject.emitted('process:focus')?.length || 0
				await pageObject.element.maximise().trigger('click')
			})

			it('Should remove "maximised" class', () => {
				expect(pageObject.element.root().classes('process-container--maximised')).toBeFalsy()
			})

			it('Should emit "process:focus" event', () => {
				expect(pageObject.emitted('process:focus')).toHaveLength(emittedCount + 1)
			})
		})
	})

	describe('And "mousedown" event triggers on toolbar element with non-primary button', () => {
		beforeAll(() => {
			pageObject.element.toolbar().trigger('mousedown', { button: 1 })
		})

		it('Should not bind functions to body events', () => {
			expect(document.body.addEventListener).toHaveBeenCalledTimes(0)
		})
	})

	describe('And "mousedown" event triggers on toolbar element of maximised process', () => {
		beforeAll(() => {
			pageObject.setRef('isMaximised', true)
			pageObject.element.toolbar().trigger('mousedown', { button: 1 })
			pageObject.setRef('isMaximised', false)
		})

		it('Should not bind functions to body events', () => {
			expect(document.body.addEventListener).toHaveBeenCalledTimes(0)
		})
	})

	describe('And "mousedown" event triggers on toolbar element with primary button', () => {
		const grabStartX = 100
		const grabStartY = 200

		beforeAll(() => {
			pageObject.element.toolbar().trigger('mousedown', { x: grabStartX, y: grabStartY })
		})

		it('Should bind functions to body events', () => {
			expect(document.body.addEventListener).toHaveBeenCalledTimes(3)
			expect(document.body.addEventListener).toHaveBeenNthCalledWith(1, 'mousemove', expect.any(Function))
			expect(document.body.addEventListener).toHaveBeenNthCalledWith(2, 'mouseup', expect.any(Function))
			expect(document.body.addEventListener).toHaveBeenNthCalledWith(3, 'mouseleave', expect.any(Function))
		})

		describe('And calling `mousemove` callback with arbitrary values', () => {
			const mockX = 40
			const mockY = 80

			beforeAll(() => {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				mockAddEventListener.mock.calls[0][1]({ x: mockX, y: mockY })
			})

			it('Should adjust process position', () => {
				const left = dimensions.container.offsetWidth / 2 - dimensions.process.offsetWidth / 2 - (grabStartX - mockX)
				const top = dimensions.container.offsetHeight / 2 - dimensions.process.offsetHeight / 2 - (grabStartY - mockY)

				expect(pageObject.element.process().attributes('style'))
					.toContain(`transform: translate(${left}px, ${top}px);`)
			})
		})

		describe('And calling `mousemove` callback with big, negative values', () => {
			const mockX = -10000
			const mockY = -10000

			beforeAll(() => {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				mockAddEventListener.mock.calls[0][1]({ x: mockX, y: mockY })
			})

			it('Should adjust process position to 0, 0', () => {
				expect(pageObject.element.process().attributes('style')).toContain('transform: translate(0px, 0px);')
			})
		})

		describe('And calling `mousemove` callback with big, positive values', () => {
			const mockX = 10000
			const mockY = 10000

			beforeAll(() => {
				// eslint-disable-next-line @typescript-eslint/ban-ts-comment
				// @ts-ignore
				mockAddEventListener.mock.calls[0][1]({ x: mockX, y: mockY })
			})

			it('Should adjust process position to max values', () => {
				const left = dimensions.container.offsetWidth - dimensions.process.offsetWidth
				const top = dimensions.container.offsetHeight - dimensions.process.offsetHeight

				expect(pageObject.element.process().attributes('style'))
					.toContain(`transform: translate(${left}px, ${top}px);`)
			})
		})

		describe('And running `setPosition` method', () => {
			const updatedContainerWidth = 1280
			const updatedContainerHeight = 620
			const updatedProcessWidth = 520
			const updatedProcessHeight = 340
			let originalContainerWidth: number
			let originalContainerHeight: number
			let originalProcessWidth: number
			let originalProcessHeight: number

			const left = updatedContainerWidth / 2 - updatedProcessWidth / 2
			const top = updatedContainerHeight / 2 - updatedProcessHeight / 2

			beforeAll(() => {
				originalContainerWidth = dimensions.container.offsetWidth
				originalContainerHeight = dimensions.container.offsetHeight
				originalProcessWidth = dimensions.process.offsetWidth
				originalProcessHeight = dimensions.process.offsetHeight

				dimensions.container.offsetWidth = updatedContainerWidth
				dimensions.container.offsetHeight = updatedContainerHeight
			})

			afterAll(() => {
				dimensions.container.offsetWidth = originalContainerWidth
				dimensions.container.offsetHeight = originalContainerHeight
				dimensions.process.offsetWidth = originalProcessWidth
				dimensions.process.offsetHeight = originalProcessHeight
			})

			it('Should recalculate size of element and re-position it', async () => {
				dimensions.process.offsetWidth = updatedProcessWidth
				dimensions.process.offsetHeight = updatedProcessHeight;

				(pageObject.wrapper.vm as ComponentPublicInstance & { setPosition: () => void }).setPosition()

				await nextTick()

				expect(pageObject.element.process().attributes('style'))
					.toContain(`transform: translate(${left}px, ${top}px);`)
			})
		})

		describe.each([
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-ignore
			['mouseup', (): void => mockAddEventListener.mock.calls[1][1]()],
			// eslint-disable-next-line @typescript-eslint/ban-ts-comment
			// @ts-ignore
			['mouseleave', (): void => mockAddEventListener.mock.calls[2][1]()],
			['onUnmounted', (): void => pageObject.wrapper.unmount()],
		])('And calling `%s` callback', (_, fn) => {
			beforeAll(() => {
				mockRemoveEventListener.mockReset()
				fn()
			})

			it('Should un-bind body events', () => {
				expect(document.body.removeEventListener).toHaveBeenCalledTimes(3)
				expect(document.body.removeEventListener).toHaveBeenNthCalledWith(1, 'mousemove', expect.any(Function))
				expect(document.body.removeEventListener).toHaveBeenNthCalledWith(2, 'mouseup', expect.any(Function))
				expect(document.body.removeEventListener).toHaveBeenNthCalledWith(3, 'mouseleave', expect.any(Function))
			})
		})
	})
})


describe('When mounting with optional props', () => {
	let pageObject: PageObject
	const file: File = {
		data: {},
		icon: '/my/icon.svg',
		meta: {
			cannotMaximise: true,
			cannotMinimise: true,
			cannotResize: true,
		},
		name: 'My File',
		type: FileType.TEXT,
	}

	beforeAll(() => {
		pageObject = setup(file)
	})

	it('Should not call new ResizeObserver', () => {
		expect(ResizeObserver).not.toHaveBeenCalled()
	})

	it('Should not show maximise button', () => {
		expect(pageObject.element.maximise().exists()).toBe(false)
	})

	it('Should not show minimise button', () => {
		expect(pageObject.element.minimise().exists()).toBe(false)
	})

	it('Should show toolbar icon', () => {
		expect(pageObject.element.icon().attributes('src')).toBe(file.icon)
	})

	describe('And double-clicking title element', () => {
		let emittedCount: number

		beforeAll(async () => {
			emittedCount = pageObject.emitted('process:focus')?.length || 0
			await pageObject.element.title().trigger('dblclick')
		})

		it('Should not add "maximised" class', () => {
			expect(pageObject.element.root().classes('process-container--maximised')).toBe(false)
		})

		it('Should emit "process:focus" event', () => {
			expect(pageObject.emitted('process:focus')).toHaveLength(emittedCount + 1)
		})
	})
})

describe('When mounting with singular default dimension', () => {
	let pageObject: PageObject
	const mockWidth = 666
	const mockHeight = 444

	describe('And dimension is `width`', () => {
		beforeAll(() => {
			pageObject = setup({
				data: {},
				meta: {
					defaultSize: {
						height: 0,
						width: mockWidth,
					},
				},
				name: 'My File',
				type: FileType.TEXT,
			})
		})

		it('Should set the element\'s size and position', () => {
			const style = pageObject.element.process().attributes('style')
			const width = mockWidth
			const height = dimensions.process.offsetHeight
			const left = dimensions.container.offsetWidth / 2 - width / 2
			const top = dimensions.container.offsetHeight / 2 - height / 2
			expect(style).toBe(`transform: translate(${left}px, ${top}px); width: ${width}px; height: ${height}px;`)
		})
	})

	describe('And dimension is `height`', () => {
		beforeAll(() => {
			pageObject = setup({
				data: {},
				meta: {
					defaultSize: {
						height: mockHeight,
						width: 0,
					},
				},
				name: 'My File',
				type: FileType.TEXT,
			})
		})

		it('Should set the element\'s size and position', () => {
			const style = pageObject.element.process().attributes('style')
			const width = dimensions.process.offsetWidth
			const height = mockHeight
			const left = dimensions.container.offsetWidth / 2 - width / 2
			const top = dimensions.container.offsetHeight / 2 - height / 2
			expect(style).toBe(`transform: translate(${left}px, ${top}px); width: ${width}px; height: ${height}px;`)
		})
	})
})
