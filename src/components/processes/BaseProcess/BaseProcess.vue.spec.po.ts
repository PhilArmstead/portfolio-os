import type { DOMWrapper, VueWrapper } from '@vue/test-utils'
import type { ComponentPublicInstance } from 'vue'

type ElementName = 'close' | 'content' | 'icon' | 'maximise' | 'minimise' | 'process' | 'title' | 'toolbar' | 'root'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get element (): Record<ElementName, () => DOMWrapper<Element>> {
		return {
			close: () => this.wrapper.find('.process__cta-close'),
			content: () => this.wrapper.find('.process__content'),
			icon: () => this.wrapper.find('.process__icon'),
			maximise: () => this.wrapper.find('.process__cta-maximise'),
			minimise: () => this.wrapper.find('.process__cta-minimise'),
			process: () => this.wrapper.find('.process'),
			root: () => this.wrapper.find('.process-container'),
			title: () => this.wrapper.find('.process__title'),
			toolbar: () => this.wrapper.find('.process__toolbar'),
		}
	}

	getRef (name: string): unknown {
		return (this.wrapper.vm as ComponentPublicInstance & Record<string, unknown>)[name]
	}

	setRef (name: string, value: unknown): void {
		(this.wrapper.vm as ComponentPublicInstance & Record<string, typeof value>)[name] = value
	}

	emitted (event: string): string[][] | undefined {
		return this.wrapper.emitted(event)
	}
}
