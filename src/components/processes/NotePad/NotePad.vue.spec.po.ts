import type { DOMWrapper, VueWrapper } from '@vue/test-utils'

import BaseProcess from '../BaseProcess/BaseProcess.vue'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'baseProcess', () => VueWrapper> {
		return {
			baseProcess: () => this.wrapper.findComponent(BaseProcess) as VueWrapper,
		}
	}

	get element (): Record<'textPoor' | 'textRich', () => DOMWrapper<Element>> {
		return {
			textPoor: () => this.wrapper.find('.notepad__text--poor'),
			textRich: () => this.wrapper.find('.notepad__text--rich'),
		}
	}
}
