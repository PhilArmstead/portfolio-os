import { beforeAll, describe, expect, it } from 'vitest'
import type { DOMWrapper } from '@vue/test-utils'
import { mount } from '@vue/test-utils'

import type { File } from '../../../../types'
import { FileType } from '../../../enums/files'
import NotePad from './NotePad.vue'
import PageObject from './NotePad.vue.spec.po'

describe('NotePad', () => {
	describe('When mounting component', () => {
		describe('And passing a rich-text file prop', () => {
			const file: File = {
				data: { content: '<h1>Hello, world!</h1>' },
				meta: {},
				name: 'My document',
				type: FileType.RICH_TEXT,
			}
			let pageObject: PageObject

			beforeAll(() => {
				pageObject = new PageObject(mount(NotePad, { props: { file } }))
			})

			it('Should pass `file` prop to <base-process />', () => {
				expect(pageObject.component.baseProcess().props()).toEqual({ file })
			})

			it('Should display content in rich-text element', () => {
				expect(pageObject.element.textRich().html()).toContain(file.data.content)
			})
		})

		describe('And passing a non-rich-text file prop', () => {
			const file: File = {
				data: { content: 'Hi, world.' },
				meta: {},
				name: 'My document',
				type: FileType.TEXT,
			}
			let pageObject: PageObject

			beforeAll(() => {
				pageObject = new PageObject(mount(NotePad, { props: { file } }))
			})

			it('Should pass `file` prop to <base-process />', () => {
				expect(pageObject.component.baseProcess().props()).toEqual({ file })
			})

			it('Should display content in non-rich-text element', () => {
				expect((pageObject.element.textPoor() as DOMWrapper<HTMLTextAreaElement>).element.value).toBe(file.data.content)
			})

			describe('And non-rich-text-element emits "change" event', () => {
				beforeAll(() => {
					pageObject.element.textPoor().setValue('blah')
				})

				it('Should update content', () => {
					expect((pageObject.element.textPoor() as DOMWrapper<HTMLTextAreaElement>).element.value).toBe('blah')
				})
			})
		})
	})
})
