import { beforeAll, describe, expect, it, vi } from 'vitest'
import { mount } from '@vue/test-utils'

import type { File } from '../../../../types'
import { FileType } from '../../../enums/files'
import ModalAlert from './ModalAlert.vue'
import PageObject from './ModalAlert.vue.spec.po'

describe('ModalAlert', () => {
	describe('When mounting component', () => {
		const file: File = {
			data: { content: 'Something has happened!' },
			meta: {},
			name: 'Warning',
			type: FileType.ALERT,
		}
		let pageObject: PageObject

		beforeAll(async () => {
			vi.spyOn(HTMLButtonElement.prototype, 'focus')
			pageObject = new PageObject(mount(ModalAlert, { props: { file } }))
		})

		it('Should pass `file` prop to <base-process />', () => {
			expect(pageObject.component.baseProcess().props()).toEqual({ file })
		})

		it('Should display content', () => {
			expect(pageObject.element.message().text()).toBe(file.data.content)
		})

		it('Should focus button element', () => {
			expect((pageObject.element.button().element as HTMLButtonElement).focus).toHaveBeenCalled()
		})

		describe('And clicking button element', () => {
			beforeAll(() => {
				pageObject.element.button().trigger('click')
			})

			it('Should emit "process:close" event', () => {
				expect(pageObject.wrapper.emitted('process:close')).toHaveLength(1)
			})
		})
	})
})
