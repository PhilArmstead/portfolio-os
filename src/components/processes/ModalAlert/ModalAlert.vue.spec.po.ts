import type { DOMWrapper, VueWrapper } from '@vue/test-utils'

import BaseProcess from '../BaseProcess/BaseProcess.vue'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'baseProcess', () => VueWrapper> {
		return {
			baseProcess: () => this.wrapper.findComponent(BaseProcess) as VueWrapper,
		}
	}

	get element (): Record<'button' | 'message', () => DOMWrapper<Element | HTMLButtonElement>> {
		return {
			button: () => this.wrapper.find('.alert__dismiss'),
			message: () => this.wrapper.find('.alert__message'),
		}
	}
}
