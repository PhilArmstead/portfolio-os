import { beforeAll, describe, expect, it, vi } from 'vitest'
import { mount } from '@vue/test-utils'

import type { File } from '../../../../types'
import { FileType } from '../../../enums/files'
import ImageViewer from './ImageViewer.vue'
import PageObject from './ImageViewer.vue.spec.po'

const imageMock = vi.spyOn(window, 'Image')

describe('ImageViewer', () => {
	describe('When mounting component', () => {
		describe('And File contains content string', () => {
			const file: File = {
				data: { content: '/path/to/image.jpg' },
				meta: {},
				name: 'My Selfie',
				type: FileType.IMAGE,
			}
			let pageObject: PageObject

			beforeAll(async () => {
				imageMock.mockReset()
				pageObject = new PageObject(mount(ImageViewer, { props: { file } }))
			})

			it('Should instantiate Image', () => {
				expect(imageMock).toHaveBeenCalledTimes(1)
				expect(imageMock.mock.instances[0].src).toBe(file.data.content)
			})

			it('Should pass `file` prop to <base-process />', () => {
				expect(pageObject.component.baseProcess().props()).toEqual({ file })
			})

			it('Should display "loading" message', () => {
				expect(pageObject.element.loading().text()).toBe('Loading...')
			})

			it('Should not display <img> element', () => {
				expect(pageObject.element.image().exists()).toBe(false)
			})

			describe('And image finishes loading', () => {
				beforeAll(async () => {
					await imageMock.mock.instances[0].onload?.(new CustomEvent(''))
				})

				it('Should not display "loading" message', () => {
					expect(pageObject.element.loading().exists()).toBe(false)
				})

				it('Should display <img> element', () => {
					expect(pageObject.element.image().attributes('src')).toBe(file.data.content)
				})
			})
		})

		describe('And File does not contain content', () => {
			const file: File = {
				data: {},
				meta: {},
				name: 'My Selfie',
				type: FileType.IMAGE,
			}

			beforeAll(async () => {
				imageMock.mockReset()
				mount(ImageViewer, { props: { file } })
			})

			it('Should not instantiate Image', () => {
				expect(imageMock).toHaveBeenCalledTimes(0)
			})
		})
	})
})
