import type { VueWrapper } from '@vue/test-utils'

import BaseProcess from '../BaseProcess/BaseProcess.vue'
import FileList from '../../FileList/FileList.vue'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'baseProcess' | 'fileList', () => VueWrapper> {
		return {
			baseProcess: () => this.wrapper.findComponent(BaseProcess) as VueWrapper,
			fileList: () => this.wrapper.findComponent(FileList) as VueWrapper,
		}
	}
}
