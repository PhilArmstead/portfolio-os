import { beforeAll, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'

import type { File } from '../../../../types'
import FileExplorer from './FileExplorer.vue'
import { FileType } from '../../../enums/files'
import PageObject from './FileExplorer.vue.spec.po'

describe('FileExplorer', () => {
	describe('When mounting component', () => {
		describe('And `file` prop contains `files` list', () => {
			const fileList: File[] = [{
				data: {},
				meta: {},
				name: 'Another file',
				type: FileType.CODE,
			}]
			const file: File = {
				data: { files: fileList },
				meta: {},
				name: 'hello-world',
				type: FileType.SHELL,
			}
			let pageObject: PageObject

			beforeAll(async () => {
				pageObject = new PageObject(mount(FileExplorer, { props: { file } }))
			})

			it('Should pass `file` prop to <base-process />', () => {
				expect(pageObject.component.baseProcess().props()).toEqual({ file })
			})

			it('Should pass props to <file-list />', () => {
				expect(pageObject.component.fileList().props()).toEqual({
					files: file.data.files,
					layout: 'icon-row',
				})
			})
		})

		describe('And `file` prop does not contain `files` list', () => {
			const file: File = {
				data: {},
				meta: {},
				name: 'hello-world',
				type: FileType.RICH_TEXT,
			}
			let pageObject: PageObject

			beforeAll(async () => {
				pageObject = new PageObject(mount(FileExplorer, { props: { file } }))
			})

			it('Should pass empty array as `files` props to <file-list />', () => {
				expect(pageObject.component.fileList().props('files')).toHaveLength(0)
			})
		})
	})
})
