import BaseProcess from './BaseProcess/BaseProcess.vue'
import CodeViewer from './CodeViewer/CodeViewer.vue'
import CommandPrompt from './CommandPrompt/CommandPrompt.vue'
import FileExplorer from './FileExplorer/FileExplorer.vue'
import ImageViewer from './ImageViewer/ImageViewer.vue'
import ModalAlert from './ModalAlert/ModalAlert.vue'
import Notepad from './NotePad/NotePad.vue'

export {
	BaseProcess,
	CodeViewer,
	CommandPrompt,
	FileExplorer,
	ImageViewer,
	ModalAlert,
	Notepad,
}
