import { beforeAll, describe, expect, it } from 'vitest'
import { mount } from '@vue/test-utils'

import CodeViewer from './CodeViewer.vue'
import type { File } from '../../../../types'
import { FileType } from '../../../enums/files'
import PageObject from './CodeViewer.vue.spec.po'

describe('CodeViewer', () => {
	describe('When mounting component', () => {
		const file: File = {
			data: { content: '<h1>Hello, world!</h1>' },
			meta: {},
			name: 'hello-world',
			type: FileType.CODE,
		}
		let pageObject: PageObject

		beforeAll(async () => {
			pageObject = new PageObject(mount(CodeViewer, { props: { file } }))
		})

		it('Should pass `file` prop to <base-process />', () => {
			expect(pageObject.component.baseProcess().props()).toEqual({ file })
		})

		it('Should display content', () => {
			expect(pageObject.element.content().html()).toContain(file.data.content)
		})
	})
})
