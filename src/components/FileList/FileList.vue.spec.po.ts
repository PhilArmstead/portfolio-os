import type { DOMWrapper, VueWrapper } from '@vue/test-utils'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get element (): Record<'root', () => DOMWrapper<Element>> {
		return {
			root: () => this.wrapper.find('.file-list'),
		}
	}

	get elements (): Record<'items' | 'itemButtons' | 'itemImages' | 'itemNames', () => DOMWrapper<Element>[]> {
		return {
			items: () => this.wrapper.findAll('.file-list__item'),
			itemButtons: () => this.wrapper.findAll('.file-list__item .file-list__button'),
			itemImages: () => this.wrapper.findAll('.file-list__item .file-list__icon'),
			itemNames: () => this.wrapper.findAll('.file-list__item .file-list__label'),
		}
	}
}
