import { beforeAll, describe, expect, it, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import type { File } from '../../../types'
import FileList from './FileList.vue'
import { FileType } from '../../enums/files'
import PageObject from './FileList.vue.spec.po'

const mockOpenFile = vi.fn()

vi.mock('../../helpers/files', () => ({
	openFile: (file: File): void => mockOpenFile(file),
}))

describe('FileList', () => {
	describe('When mounting component', () => {
		let pageObject: PageObject

		beforeAll(() => {
			pageObject = new PageObject(shallowMount(FileList, {
				props: {
					files: [],
				},
			}))
		})

		it('Should add layout class to root element', () => {
			expect(pageObject.element.root().classes()).toEqual(['file-list--layout-icon-row', 'file-list'])
		})

		it('Should not display file elements', () => {
			expect(pageObject.elements.items()).toHaveLength(0)
		})

		describe('And changing `layout` prop', () => {
			it.each([
				['icon-column'],
				['icon-row'],
				['details'],
			])('Should set add "%s" to class', async (layout) => {
				await pageObject.wrapper.setProps({ layout })
				expect(pageObject.element.root().classes()).toEqual([`file-list--layout-${layout}`, 'file-list'])
			})
		})

		describe('And changing `files` prop', () => {
			const file1: File = {
				data: {},
				icon: '/path/to/image.svg',
				meta: {},
				name: 'My Image',
				type: FileType.IMAGE,
			}
			const file2: File = {
				data: {},
				meta: {},
				name: 'My document',
				type: FileType.RICH_TEXT,
			}

			beforeAll(async () => {
				await pageObject.wrapper.setProps({ files: [file1, file2] })
			})

			it('Should display file images', () => {
				const images = pageObject.elements.itemImages()
				expect(images.at(0)?.attributes('src')).toBe(file1.icon)
				expect(images.at(1)?.attributes('src')).toBe('/assets/icons/notepad.svg')
			})

			it('Should display file names', () => {
				const names = pageObject.elements.itemNames()
				expect(names.at(0)?.text()).toBe(file1.name)
				expect(names.at(1)?.text()).toBe(file2.name)
			})

			describe('And double-clicking file item', () => {
				beforeAll(async () => {
					await pageObject.elements.itemButtons().at(0)?.trigger('dblclick')
				})

				it('Should call `openFile`', () => {
					expect(mockOpenFile).toHaveBeenCalledWith(file1)
				})
			})

			describe('And pressing enter on file item', () => {
				beforeAll(async () => {
					await pageObject.elements.itemButtons().at(1)?.trigger('keydown.enter')
				})

				it('Should call `openFile`', () => {
					expect(mockOpenFile).toHaveBeenCalledWith(file2)
				})
			})
		})
	})
})
