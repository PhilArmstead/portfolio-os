import type { VueWrapper } from '@vue/test-utils'

import FileList from '../FileList/FileList.vue'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'fileList', () => VueWrapper> {
		return {
			fileList: () => this.wrapper.findComponent(FileList),
		}
	}

	get components (): Record<'processes', () => VueWrapper[]> {
		return {
			processes: () => this.wrapper.findAllComponents('.desktop__process') as VueWrapper[],
		}
	}
}
