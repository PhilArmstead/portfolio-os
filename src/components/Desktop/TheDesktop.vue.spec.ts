import { beforeAll, describe, expect, it, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import { processes, processZIndex } from '../../store/processes'
import { FileType } from '../../enums/files'
import PageObject from './TheDesktop.vue.spec.po'
import TheDesktop from './TheDesktop.vue'

vi.mock('../../data/directoryListings', () => ({
	desktop: [
		{ data: {}, meta: {}, name: 'File 1', type: FileType.CODE },
		{ data: {}, meta: {}, name: 'File 2', type: FileType.ALERT },
		{ data: {}, meta: {}, name: 'File 3', type: FileType.TEXT },
	],
}))

describe('TheDesktop', () => {
	let pageObject: PageObject
	const files = [
		{ data: {}, meta: {}, name: 'File 1', type: FileType.CODE },
		{ data: {}, meta: {}, name: 'File 2', type: FileType.ALERT },
		{ data: {}, meta: {}, name: 'File 3', type: FileType.TEXT },
	]

	beforeAll(() => {
		pageObject = new PageObject(shallowMount(TheDesktop))
	})

	it('Should embed <file-list> component', () => {
		expect(pageObject.component.fileList().props()).toEqual({
			files,
			layout: 'icon-column',
		})
	})

	it('Should not embed process components', () => {
		expect(pageObject.components.processes()).toHaveLength(0)
	})

	describe('And adding processes', () => {
		beforeAll(() => {
			processZIndex.value = 5
			processes.push({
				component: {
					name: 'Test Component 1',
					template: '<div></div>',
					props: ['file', 'id'],
				},
				file: files[0],
				id: 2,
				zIndex: 3,
			})
			processes.push({
				component: {
					name: 'Test Component 2',
					template: '<span></span>',
					props: ['file', 'id'],
				},
				file: files[1],
				id: 3,
				zIndex: 4,
			})
		})

		it('Should embed process components', () => {
			const processes = pageObject.components.processes()
			expect(processes).toHaveLength(2)

			expect(processes.at(0)?.props()).toEqual({
				file: files[0],
				id: 'process-2',
			})
			const attributes1 = processes.at(0)?.attributes()
			expect(attributes1?.class).toBe('desktop__process')
			expect(attributes1?.style).toBe('z-index: 3;')

			expect(processes.at(1)?.props()).toEqual({
				file: files[1],
				id: 'process-3',
			})
			const attributes2 = processes.at(1)?.attributes()
			expect(attributes2?.class).toBe('process-container--focused desktop__process')
			expect(attributes2?.style).toBe('z-index: 4;')
		})

		describe.each([
			['process:close', 1],
			['process:focus', 3],
			['process:minimise', 3],
		])('And process component emits "%s" event', (eventName, payload) => {
			beforeAll(async () => {
				await pageObject.components.processes().at(1)?.trigger(eventName)
			})

			it(`Should emit "${eventName}" event`, () => {
				expect(pageObject.wrapper.emitted(eventName)).toEqual([[payload]])
			})
		})
	})
})
