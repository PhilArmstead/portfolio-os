import { beforeAll, describe, expect, it, vi } from 'vitest'
import type { Component } from 'vue'
import { shallowMount } from '@vue/test-utils'

import { processes, processZIndex } from '../../store/processes'
import type { File } from '../../../types'
import { FileType } from '../../enums/files'
import PageObject from './TheTaskbar.vue.spec.po'
import TheTaskbar from './TheTaskbar.vue'

const mockOpenFile = vi.fn()

vi.mock('../../helpers/files', () => ({
	openFile: (file: File): void => mockOpenFile(file),
}))

describe('TheTaskbar', () => {
	describe('When mounting component', () => {
		let pageObject: PageObject

		beforeAll(() => {
			pageObject = new PageObject(shallowMount(TheTaskbar))
		})

		it('Should embed start button element', () => {
			expect(pageObject.element.startButton().text()).toBe('Start')
		})

		it('Should mount <taskbar-clock /> component', () => {
			expect(pageObject.component.clock().exists()).toBe(true)
		})

		it('Should not embed processes', () => {
			expect(pageObject.elements.processes()).toHaveLength(0)
		})

		describe('And clicking start button', () => {
			beforeAll(async () => {
				await pageObject.element.startButton().trigger('click')
			})

			it('Should call `openFile`', () => {
				expect(mockOpenFile).toHaveBeenCalledWith({
					data: { content: 'Can you imagine! (Maybe one day.)' },
					meta: {},
					name: 'Error: functionality missing',
					type: FileType.ALERT,
				})
			})
		})

		describe('And adding processes', () => {
			const component: Component = {
				name: 'my-component',
				template: '<main class="a-component" />',
			}
			const file1: File = {
				data: {},
				meta: {},
				name: 'Mock process 1',
				type: FileType.ALERT,
			}
			const file2: File = {
				data: {},
				icon: '/path/to/process2.jpg',
				meta: {},
				name: 'Mock process 2',
				type: FileType.CODE,
			}
			const file3: File = {
				data: {},
				icon: '/process3.svg',
				meta: {},
				name: 'Mock process 3',
				type: FileType.RICH_TEXT,
			}
			beforeAll(() => {
				processZIndex.value = 16
				processes.push({ component, file: file1, id: 1, isMinimised: false, zIndex: 1 })
				processes.push({ component, file: file2, id: 2, isMinimised: false, zIndex: processZIndex.value - 1 })
				processes.push({ component, file: file3, id: 3, isMinimised: true, zIndex: 6 })
			})

			it('Should embed process elements', () => {
				expect(pageObject.elements.processes()).toHaveLength(3)
			})

			it('Should add focus class to element with `zIndex` one below `processZIndex`', () => {
				expect(pageObject.elements.processes().at(0)?.classes()).not.toContain('process-list__process--focused')
				expect(pageObject.elements.processes().at(1)?.classes()).toContain('process-list__process--focused')
				expect(pageObject.elements.processes().at(2)?.classes()).not.toContain('process-list__process--focused')
			})

			it('Should embed process icons', () => {
				expect(pageObject.elements.processes().at(0)?.find('img').attributes('src')).toBe('/assets/icons/alert.svg')
				expect(pageObject.elements.processes().at(1)?.find('img').attributes('src')).toBe(file2.icon)
				expect(pageObject.elements.processes().at(2)?.find('img').attributes('src')).toBe(file3.icon)
			})

			describe('And clicking process button', () => {
				beforeAll(async () => {
					await pageObject.elements.processes().at(2)?.trigger('click')
				})

				it('Should emit "taskbar:application-click" event', () => {
					expect(pageObject.wrapper.emitted('taskbar:application-click')).toEqual([[3]])
				})
			})
		})
	})
})
