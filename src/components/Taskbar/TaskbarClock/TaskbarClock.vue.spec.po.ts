import type { DOMWrapper, VueWrapper } from '@vue/test-utils'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get element (): Record<'date' | 'time', () => DOMWrapper<Element>> {
		return {
			date: () => this.wrapper.find('.taskbar-clock__date'),
			time: () => this.wrapper.find('.taskbar-clock__time'),
		}
	}
}
