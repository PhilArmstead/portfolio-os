import { DAYS_OF_WEEK } from './constants'
import { operatingSystem } from '../../../store/client'


// TODO: fix timezone
export const parseTime = (now: Date): string => {
	const time = now.toLocaleTimeString()
	return time.substring(0, time.lastIndexOf(':'))
}
export let parseDate: (now: Date) => string

switch (operatingSystem) {
	case 'mac':
		parseDate = (now: Date): string => DAYS_OF_WEEK[now.getDay()]
		break
	default:
		parseDate = (now: Date): string => now.toLocaleDateString()
}
