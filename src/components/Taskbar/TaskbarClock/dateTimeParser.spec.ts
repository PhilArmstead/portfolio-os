import { beforeEach, describe, expect, it, vi } from 'vitest'

const mockClientStore = { operatingSystem: '' }
vi.mock('../../../store/client', () => mockClientStore)

const now = new Date(2004, 1, 14, 12, 31, 15, 19)
vi.useFakeTimers()
vi.setSystemTime(now)

describe('parseDate', () => {
	beforeEach(() => {
		vi.resetModules()
	})

	describe('When `operatingSystem` is "mac"', () => {
		it('Should return the day of the week', async () => {
			mockClientStore.operatingSystem = 'mac'
			const { parseDate } = await vi.importActual<{ parseDate: (now: Date) => string }>('./dateTimeParser')
			expect(parseDate(now)).toBe('Sat')
		})
	})

	describe('When `operatingSystem` is anything else', () => {
		it('Should return the date as string', async () => {
			mockClientStore.operatingSystem = 'something else'
			const { parseDate } = await vi.importActual<{ parseDate: (now: Date) => string }>('./dateTimeParser')
			expect(parseDate(now)).toBe('14/02/2004')
		})
	})
})

describe('parseTime', () => {
	it('Should Should return the time as HH:mm:ss', async () => {
		const { parseTime } = await vi.importActual<{ parseTime: (now: Date) => string }>('./dateTimeParser')
		expect(parseTime(now)).toBe('12:31')
	})
})
