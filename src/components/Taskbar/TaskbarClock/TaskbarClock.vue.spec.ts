import { beforeAll, describe, expect, it, vi } from 'vitest'
import { shallowMount } from '@vue/test-utils'

import PageObject from './TaskbarClock.vue.spec.po'
import TaskbarClock from './TaskbarClock.vue'

const now = new Date(2004, 1, 14, 12, 31, 15, 19)
vi.useFakeTimers()
vi.setSystemTime(now)

describe('TaskbarClock', () => {
	describe('When mounting component', () => {
		let pageObject: PageObject

		beforeAll(() => {
			pageObject = new PageObject(shallowMount(TaskbarClock))
		})

		it('Should display time', () => {
			expect(pageObject.element.time().text()).toBe('12:31')
		})

		it('Should display date', () => {
			expect(pageObject.element.date().text()).toBe('14/02/2004')
		})

		describe('When time advances by a minute', () => {
			beforeAll(() => {
				const then = new Date()
				then.setUTCMinutes(32)
				vi.setSystemTime(then)
				vi.advanceTimersByTime(60 - then.getUTCSeconds())
			})

			it('Should update time', () => {
				expect(pageObject.element.time().text()).toBe('12:32')
			})
		})
	})
})
