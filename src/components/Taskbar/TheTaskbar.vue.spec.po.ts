import type { DOMWrapper, VueWrapper } from '@vue/test-utils'

export default class PageObject {
	wrapper: VueWrapper

	constructor (wrapper: VueWrapper) {
		this.wrapper = wrapper
	}

	get component (): Record<'clock', () => ReturnType<VueWrapper['findComponent']>> {
		return {
			clock: () => this.wrapper.findComponent('.taskbar__clock'),
		}
	}

	get element (): Record<'startButton', () => DOMWrapper<Element>> {
		return {
			startButton: () => this.wrapper.find('.taskbar__start'),
		}
	}

	get elements (): Record<'processes', () => DOMWrapper<Element>[]> {
		return {
			processes: () => this.wrapper.findAll('.process-list__process'),
		}
	}
}
