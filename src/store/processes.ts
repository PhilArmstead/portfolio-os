import { ref, shallowReactive } from 'vue'
import type { Component } from 'vue'

import type { File, Process } from '../../types'

export const processes: Process[] = shallowReactive([])
const processId = ref(1)
export const processZIndex = ref(0)

export const createProcess = (file: File, component: Component): void => {
	processes.push({ component, file, id: processId.value, zIndex: processZIndex.value })
	++processId.value
	++processZIndex.value
}

export const blurActiveProcess = (): void => {
	processZIndex.value++
}

export const removeProcess = (index: number): void => {
	processes.splice(index, 1)

	if (!processes.length) {
		processZIndex.value = 0
	}
}

export const setProcessFocus = (clickedId: number): void => {
	const process = processes.find(({ id }) => id === clickedId)

	if (!process) {
		return
	}

	if (process.zIndex <= processZIndex.value - 2) {
		process.zIndex = processZIndex.value
		++processZIndex.value
	}
}

export const minimiseToggle = (clickedId: number): void => {
	const process = processes.find(({ id }) => id === clickedId)

	if (!process) {
		return
	}

	const { isMinimised, file: { meta }, zIndex } = process

	if (meta.cannotMinimise || isMinimised || zIndex <= processZIndex.value - 2) {
		setProcessFocus(clickedId)
		setMinimisedStatus(clickedId, false)
	} else {
		setMinimisedStatus(process.id, true)
		if (process.zIndex === processZIndex.value - 1) {
			process.zIndex = processZIndex.value - 2
		}
	}
}

export const setMinimisedStatus = (id: number, value: boolean): void => {
	document.getElementById(`process-${id}`)
	// TODO: put event names in enum
		?.dispatchEvent(new CustomEvent('process:set-is-minimised', { detail: value }))
}
