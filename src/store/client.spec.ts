import { describe, expect, it, vi } from 'vitest'

const userAgent = vi.spyOn(window.navigator, 'userAgent', 'get')

describe('operatingSystem', () => {
	describe('When `navigator.userAgent` contains "Mac"', () => {
		it('Should return "mac"', async () => {
			vi.resetModules()
			userAgent.mockReturnValue('Running MacOs')
			const { operatingSystem } = await vi.importActual<{ operatingSystem: string }>('./client')
			expect(operatingSystem).toBe('mac')
		})
	})

	describe('When `navigator.userAgent` contains "Win"', () => {
		it('Should return "win"', async () => {
			vi.resetModules()
			userAgent.mockReturnValue('I am on Win7')
			const { operatingSystem } = await vi.importActual<{ operatingSystem: string }>('./client')
			expect(operatingSystem).toBe('win')
		})
	})
})
