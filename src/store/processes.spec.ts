import { afterAll, beforeAll, describe, expect, it, vi } from 'vitest'

import { isRef, isShallow } from 'vue'
import type { Component } from 'vue'

import {
	blurActiveProcess,
	createProcess,
	minimiseToggle,
	processes,
	processZIndex,
	removeProcess, setMinimisedStatus, setProcessFocus,
} from './processes'
import type { File, Process } from '../../types'
import { FileType } from '../enums/files'

const mockGetElementById = vi.spyOn(document, 'getElementById')
const mockCustomEvent = vi.spyOn(window, 'CustomEvent')

const file: File = {
	data: {},
	meta: {},
	name: 'My file',
	type: FileType.ALERT,
}
const component: Component = {
	name: 'My component',
	template: '<div />',
}
const MOCK_PROCESS: Omit<Process, 'id'> = {
	component: { name: 'Component', template: '' },
	file,
	isMinimised: false,
	zIndex: 1,
}

describe('processes', () => {
	it('Should be an empty array', () => {
		expect(Array.isArray(processes)).toBe(true)
		expect(processes).toHaveLength(0)
	})

	it('Should be shallowReactive', () => {
		expect(isShallow(processes)).toBe(true)
	})
})

describe('processZIndex', () => {
	it('Should be 0', () => {
		expect(processZIndex.value).toBe(0)
	})

	it('Should be ref', () => {
		expect(isRef(processZIndex)).toBe(true)
	})
})

describe('createProcess', () => {


	beforeAll(() => {
		createProcess(file, component)
	})

	it('Should add to `processes`', () => {
		expect(processes).toEqual([{
			component,
			file,
			id: 1,
			zIndex: 0,
		}])
	})

	describe('When calling again', () => {
		beforeAll(() => {
			createProcess(file, component)
		})

		it('Should increment `id` and `zIndex`', () => {
			expect(processes).toEqual([
				{
					component,
					file,
					id: 1,
					zIndex: 0,
				},
				{
					component,
					file,
					id: 2,
					zIndex: 1,
				},
			])
		})
	})
})

describe('blurActiveProcess', () => {
	let initialZIndex: number

	beforeAll(() => {
		initialZIndex = processZIndex.value
	})

	afterAll(() => {
		processZIndex.value = initialZIndex
	})

	it('Should increment `processZIndex`', () => {
		blurActiveProcess()
		expect(processZIndex.value).toBe(initialZIndex + 1)
		blurActiveProcess()
		expect(processZIndex.value).toBe(initialZIndex + 2)
		blurActiveProcess()
		expect(processZIndex.value).toBe(initialZIndex + 3)

	})
})

describe('removeProcess', () => {
	let initialZIndex: number

	beforeAll(() => {
		initialZIndex = processZIndex.value
	})

	afterAll(() => {
		processZIndex.value = initialZIndex
	})

	describe('When `processes` contains two elements', () => {
		let process1: Process

		beforeAll(() => {
			processZIndex.value = 4
			processes.length = 0
			createProcess(file, component)
			createProcess(file, component)

			process1 = processes[0]

			removeProcess(1)
		})

		it('Should reduce size of `processes` by 1', () => {
			expect(processes).toHaveLength(1)
		})

		it('Should not delete other index', () => {
			expect(processes[0]).toEqual(process1)
		})

		it('Should not reset `processZIndex`', () => {
			expect(processZIndex.value).toBe(6)
		})
	})

	describe('When `processes` contains one element', () => {
		beforeAll(() => {
			processZIndex.value = 7
			processes.length = 0
			createProcess(file, component)
			removeProcess(0)
		})

		it('Should reduce size of `processes` by 1', () => {
			expect(processes).toHaveLength(0)
		})

		it('Should reset `processZIndex`', () => {
			expect(processZIndex.value).toBe(0)
		})
	})
})

describe('setMinimisedStatus', () => {
	describe.each([
		[9, true],
		[4, false],
	])('When passing `id` of %s and `value` of %s', (id, value) => {
		const mockDispatchEvent = vi.fn()
		let customEvent: CustomEvent

		beforeAll(() => {
			mockCustomEvent.mockReset()
			const el = document.createElement('p')
			el.dispatchEvent = mockDispatchEvent
			mockGetElementById.mockReturnValue(el)
			setMinimisedStatus(id, value)
			customEvent = mockCustomEvent.mock.instances[0]
		})

		it('Should call `getElementById` on process', () => {
			expect(document.getElementById).toHaveBeenCalledWith(`process-${id}`)
		})

		it('Should create CustomEvent', () => {
			expect(mockCustomEvent).toHaveBeenCalledWith('process:set-is-minimised', { detail: value })
		})

		it('Should call `dispatchEvent` on process', () => {
			expect(mockDispatchEvent).toHaveBeenCalledWith(customEvent)
		})
	})
})

describe('setProcessFocus', () => {
	describe('When process does not exist', () => {
		beforeAll(() => {
			processes.length = 0
			processZIndex.value = 13
			setProcessFocus(0)
		})

		it('Should not increment `processZIndex`', () => {
			expect(processZIndex.value).toBe(13)
		})
	})

	describe('When process.zIndex > `processZIndex` - 2', () => {
		beforeAll(() => {
			processes.length = 0
			processes.push({ ...MOCK_PROCESS, id: 1, zIndex: 6 })
			processZIndex.value = 7
			setProcessFocus(processes[0].id)
		})

		it('Should not increment `processZIndex`', () => {
			expect(processZIndex.value).toBe(7)
		})
	})

	describe('When process.zIndex === `processZIndex` - 2', () => {
		beforeAll(() => {
			processes.length = 0
			processes.push({ ...MOCK_PROCESS, id: 1, zIndex: 5 })
			processZIndex.value = 7
			setProcessFocus(processes[0].id)
		})

		it('Should update `process.zIndex`', () => {
			expect(processes[0].zIndex).toBe(7)
		})

		it('Should increment `processZIndex`', () => {
			expect(processZIndex.value).toBe(8)
		})
	})
})

describe('minimiseToggle', () => {
	describe('When process does not exist', () => {
		beforeAll(() => {
			processes.length = 0
			minimiseToggle(0)
		})

		it('Should not change `processes`', () => {
			expect(processes).toHaveLength(0)
		})
	})

	describe.each([
		['cannot be minimised', { file: { ...MOCK_PROCESS.file, meta: { cannotMinimise: true } } }],
		['is minimised', { isMinimised: true }],
		['process.zIndex <= processZIndex - 2', { zIndex: 5 }],
	])('When process %s minimised', (_, process) => {
		const mockDispatchEvent = vi.fn()
		let customEvent: CustomEvent

		beforeAll(() => {
			const ID = 4
			processes.length = 0

			processes.push({ ...MOCK_PROCESS, id: ID, ...process })
			processZIndex.value = 7

			mockCustomEvent.mockReset()
			const el = document.createElement('p')
			el.dispatchEvent = mockDispatchEvent
			mockGetElementById.mockReturnValue(el)
			minimiseToggle(ID)
			customEvent = mockCustomEvent.mock.instances[0]
		})

		it('Should update process.zIndex', () => {
			expect(processes[0].zIndex).toBe(7)
		})

		it('Should increment `processZIndex`', () => {
			expect(processZIndex.value).toBe(8)
		})

		it('Should create CustomEvent', () => {
			expect(mockCustomEvent).toHaveBeenCalledWith('process:set-is-minimised', { detail: false })
		})

		it('Should call `dispatchEvent` on process', () => {
			expect(mockDispatchEvent).toHaveBeenCalledWith(customEvent)
		})
	})

	describe('When process is active', () => {
		describe('And process.zIndex !== `processZIndex` - 1', () => {
			const mockDispatchEvent = vi.fn()
			let customEvent: CustomEvent
			const Z_INDEX = 7

			beforeAll(() => {
				const ID = 4
				processes.length = 0
				processes.push({ ...MOCK_PROCESS, id: ID, zIndex: Z_INDEX })
				processZIndex.value = Z_INDEX

				mockCustomEvent.mockReset()
				const el = document.createElement('p')
				el.dispatchEvent = mockDispatchEvent
				mockGetElementById.mockReturnValue(el)
				minimiseToggle(ID)
				customEvent = mockCustomEvent.mock.instances[0]
			})

			it('Should create CustomEvent', () => {
				expect(mockCustomEvent).toHaveBeenCalledWith('process:set-is-minimised', { detail: true })
			})

			it('Should call `dispatchEvent` on process', () => {
				expect(mockDispatchEvent).toHaveBeenCalledWith(customEvent)
			})

			it('Should not update process.zIndex', () => {
				expect(processes[0].zIndex).toBe(Z_INDEX)
			})
		})

		describe('And process.zIndex === `processZIndex` - 1', () => {
			const mockDispatchEvent = vi.fn()
			let customEvent: CustomEvent
			const Z_INDEX = 7

			beforeAll(() => {
				const ID = 4
				processes.length = 0
				processes.push({ ...MOCK_PROCESS, id: ID, zIndex: Z_INDEX - 1 })
				processZIndex.value = Z_INDEX

				mockCustomEvent.mockReset()
				const el = document.createElement('p')
				el.dispatchEvent = mockDispatchEvent
				mockGetElementById.mockReturnValue(el)
				minimiseToggle(ID)
				customEvent = mockCustomEvent.mock.instances[0]
			})

			it('Should create CustomEvent', () => {
				expect(mockCustomEvent).toHaveBeenCalledWith('process:set-is-minimised', { detail: true })
			})

			it('Should call `dispatchEvent` on process', () => {
				expect(mockDispatchEvent).toHaveBeenCalledWith(customEvent)
			})

			it('Should update process.zIndex', () => {
				expect(processes[0].zIndex).toBe(Z_INDEX - 2)
			})
		})
	})
})
