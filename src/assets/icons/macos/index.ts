import MacIconClose from './close.svg'
import MacIconDirectory from './directory.svg'
import MacIconMaximise from './maximise.svg'
import MacIconMinimise from './minimise.svg'

export {
	MacIconClose,
	MacIconDirectory,
	MacIconMaximise,
	MacIconMinimise,
}
