import WindowsIconClose from './close.svg'
import WindowsIconDirectory from './directory.svg'
import WindowsIconMaximise from './maximise.svg'
import WindowsIconMinimise from './minimise.svg'

export {
	WindowsIconClose,
	WindowsIconDirectory,
	WindowsIconMaximise,
	WindowsIconMinimise,
}
