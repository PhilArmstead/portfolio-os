import IconAlert from './alert.svg'
import IconCode from './code.svg'
import IconDocument from './odt.svg'
import IconMe from './me.jpg'
import IconMeAndDave from './meAndDave.jpg'
import IconNetworkDisconnected from './network-disconnected.svg'
import IconNotepad from './notepad.svg'
import IconShell from './shell.svg'

export {
	IconAlert,
	IconCode,
	IconDocument,
	IconMe,
	IconMeAndDave,
	IconNetworkDisconnected,
	IconNotepad,
	IconShell,
}
