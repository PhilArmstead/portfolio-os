export const FileType = {
	TEXT: 1,
	RICH_TEXT: 2,
	IMAGE: 3,
	CODE: 4,
	SHELL: 5,
	DIRECTORY: 6,
	ALERT: 7,
}
export type FileType = typeof FileType[keyof typeof FileType]
