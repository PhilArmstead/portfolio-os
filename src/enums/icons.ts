import { IconAlert, IconCode, IconNotepad, IconShell } from '../assets/icons'
import { FileType } from './files'

export const ProcessIcons: Record<FileType, string> = {
	[FileType.ALERT]: IconAlert,
	[FileType.CODE]: IconCode,
	[FileType.RICH_TEXT]: IconNotepad,
	[FileType.SHELL]: IconShell,
	[FileType.TEXT]: IconNotepad,
}
