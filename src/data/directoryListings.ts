// TODO: I hate this file's name

import * as file from './fileContents'
import { IconDocument, IconMe } from '../assets/icons'
import type { File } from '../../types'
import { FileType } from '../enums/files'
import { ImageMe } from '../assets/images'

const presets: Record<string, Record<string, File>> = {
	images: {
		me: {
			data: { content: ImageMe },
			icon: IconMe,
			meta: { defaultSize: { height: 478, width: 450 } },
			name: 'me.jpg',
			type: FileType.IMAGE,
		},
		// meAndDave: {
		// 	name: 'me and dave.jpg',
		// 	icon: IconMeAndDave,
		// 	type: FileType.IMAGE,
		// 	meta: { file: ImageMeAndDave, dimensions: { width: 642, height: 603 } },
		// },
	},
}

// TODO: directories opening directories should not launch new processes; add history etc.
const fibonacci: File[] = [
	{ name: 'fibonacci.asm', type: FileType.CODE, data: { content: file.fibonacciAsm }, meta: { readonly: true } },
	{ name: 'fibonacci', type: FileType.SHELL, data: { content: file.fibonacciExe }, meta: {} },
]
const fizzBuzz: File[] = [
	{ name: 'fizzBuzz.asm', type: FileType.CODE, data: { content: file.fizzBuzzAsm }, meta: { readonly: true } },
	{ name: 'fizzBuzz', type: FileType.SHELL, data: { content: file.fizzBuzzExe }, meta: {} },
]
const echo: File[] = [
	{ name: 'echo.asm', type: FileType.CODE, data: { content: file.echoAsm }, meta: { readonly: true } },
	{ name: 'echo', type: FileType.SHELL, data: { content: file.echoExe }, meta: {} },
]
const assemblyCommon: File[] = [
	{ name: 'functions.asm', type: FileType.CODE, data: { content: file.functionsAsm }, meta: {} },
]
// const webFooFighters = [
// 	presets.images.meAndDave,
// ]
const assembly: File[] = [
	{ name: 'common', type: FileType.DIRECTORY, data: { files: assemblyCommon }, meta: {} },
	{ name: 'Echo', type: FileType.DIRECTORY, data: { files: echo }, meta: {} },
	{ name: 'Fibonacci', type: FileType.DIRECTORY, data: { files: fibonacci }, meta: {} },
	{ name: 'FizzBuzz', type: FileType.DIRECTORY, data: { files: fizzBuzz }, meta: {} },
]
// const webStuff: File[] = [
// 	{ name: 'Foo Fighters', type: FileType.DIRECTORY, meta: { files: webFooFighters } },
// ]
const myDocuments: File[] = [
	{ ...presets.images.me, name: 'me (copy).jpg' },
	{ name: 'Assembly', type: FileType.DIRECTORY, data: { files: assembly }, meta: {} },
	// { name: 'web stuff', type: FileType.DIRECTORY, data: { files: webStuff }, meta: {} },
]
export const desktop: File[] = [
	{ name: 'My Documents', type: FileType.DIRECTORY, data: { files: myDocuments }, meta: {} },
	presets.images.me,
	{ name: 'about-me.txt', type: FileType.TEXT, data: { content: file.aboutMeTxt }, meta: { readonly: true } },
	{ name: 'TODO.md', type: FileType.RICH_TEXT, data: { content: file.todoMd }, meta: { readonly: true } },
	// TODO: change `type` to document; inner content should not be unfeasibly big when maximised
	{ name: 'cv.odt', icon: IconDocument, type: FileType.RICH_TEXT, data: { content: file.cvOdt }, meta: {} },
]
