export const fibonacciAsm = `
<span class='comment'>; "Start with Fibonacci", they said.</span>
<span class='comment'>; "It's a great introductory task," they said.</span>
<span class='comment'>; They don't tell you that assembly language has no in-built way of printing numbers.</span>
<span class='comment'>; They don't tell you that part.</span>

<span class='comment'>; To assemble and run:</span>
<span class='comment'>;	nasm -felf64 fibonacci.asm && ld fibonacci.o -o fibonacci && ./fibonacci</span>

%include	<span class='string'>'../common/functions.asm'</span>

	<span class='keyword'>section .text</span>
	<span class='keyword'>global</span> <span class='function'>_start</span>
<span class='function'>_start:</span>
	<span class='keyword'>call</span>	<span class='function'>printNewLine</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>intro_1:</span>	<span class='comment'>; Print "Fibonacci sequence"</span>
	<span class='keyword'>call</span>	<span class='function'>printLine</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>intro_2:</span>	<span class='comment'>; Print "------------------"</span>
	<span class='keyword'>call</span>	<span class='function'>printLine</span>

	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>sequenceZero</span>	<span class='comment'>; Print "0: 0"</span>
	<span class='keyword'>call</span>	<span class='function'>printLine</span>

	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> <span class='number'>1</span>		<span class='comment'>; RAX will store current fibonacci value</span>
	<span class='keyword'>xor</span>	rbx<span class='keyword'>,</span> rbx	<span class='comment'>; RBX will store last fibonacci</span>
	<span class='keyword'>xor</span>	rcx<span class='keyword'>,</span> rcx
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> <span class='number'>10</span>		<span class='comment'>; Base of the ints we're printing</span>

<span class='function'>loopStart:</span>
	<span class='keyword'>inc</span>	rcx

	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rcx	<span class='comment'>; Print "N: " (where N is counter)</span>
	<span class='keyword'>call</span>	<span class='function'>printNumber</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>sequenceBreak</span>
	<span class='keyword'>call</span>	<span class='function'>print</span>

	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rax	<span class='comment'>; Print fibonacci result</span>
	<span class='keyword'>call</span>	<span class='function'>printNumber</span>

	<span class='keyword'>mov</span>	rdx<span class='keyword'>,</span> rax	<span class='comment'>; Increment fibonacci answers</span>
	<span class='keyword'>add</span>	rax<span class='keyword'>,</span> rbx
	<span class='keyword'>mov</span>	rbx<span class='keyword'>,</span> rdx

	<span class='keyword'>call</span>	<span class='function'>printNewLine</span>

	<span class='keyword'>cmp</span>	rcx<span class='keyword'>,</span> <span class='number'>50</span>		<span class='comment'>; Continue loop until we have done 50</span>
	<span class='keyword'>jl</span>	<span class='function'>loopStart</span>


	<span class='keyword'>call</span>	<span class='function'>quit</span>

	<span class='keyword'>section .rodata</span>
<span class='function'>intro_1:</span>	<span class='keyword'>db</span>	<span class='string'>"Fibonacci sequence"</span><span class='keyword'>,</span>	<span class='number'>0</span>
<span class='function'>intro_2:</span>	<span class='keyword'>db</span>	<span class='string'>"------------------"</span><span class='keyword'>,</span>	<span class='number'>0</span>
<span class='function'>sequenceZero:</span>	<span class='keyword'>db</span>	<span class='string'>"0: 0"</span><span class='keyword'>,</span>			<span class='number'>0</span>
<span class='function'>sequenceBreak:</span>	<span class='keyword'>db</span>	<span class='string'>": "</span><span class='keyword'>,</span>			<span class='number'>0</span>
		`
export const fibonacciExe = `
<span class='bold pink'>phil</span>@<span class='bold number'>os</span>:<span class='bold string'>~/My Documents/Assembly/Fibonacci</span>
<span class='bold string'>$</span> ./fibonacci 

Fibonacci sequence
------------------
0: 0
1: 1
2: 1
3: 2
4: 3
5: 5
6: 8
7: 13
8: 21
9: 34
10: 55
11: 89
12: 144
13: 233
14: 377
15: 610
16: 987
17: 1597
18: 2584
19: 4181
20: 6765
21: 10946
22: 17711
23: 28657
24: 46368
25: 75025
26: 121393
27: 196418
28: 317811
29: 514229
30: 832040
31: 1346269
32: 2178309
33: 3524578
34: 5702887
35: 9227465
36: 14930352
37: 24157817
38: 39088169
39: 63245986
40: 102334155
41: 165580141
42: 267914296
43: 433494437
44: 701408733
45: 1134903170
46: 1836311903
47: 2971215073
48: 4807526976
49: 7778742049
50: 12586269025
			`
export const fizzBuzzAsm = `
<span class='comment'>; Here we go</span>
<span class='comment'>; Fizz Buzz sequencer in ASM</span>

<span class='comment'>; To assemble and run:</span>
<span class='comment'>;	nasm -felf64 fizzBuzz.asm && ld fizzBuzz.o -o fizzBuzz && ./fizzBuzz</span>

%include	<span class='string'>'../functions.asm'</span>

	<span class='keyword'>section .text</span>
	<span class='keyword'>global</span> <span class='function'>_start</span>
<span class='function'>_start:</span>
	<span class='keyword'>call</span>	<span class='function'>printNewLine</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>intro_1</span>	<span class='comment'>; Print "Fizz Buzz"</span>
	<span class='keyword'>call</span>	<span class='function'>printLine</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>intro_2</span>	<span class='comment'>; Print "---------"</span>
	<span class='keyword'>call</span>	<span class='function'>print</span>

	<span class='keyword'>xor</span>	rcx<span class='keyword'>,</span> rcx
	<span class='keyword'>lea</span>	rdi<span class='keyword'>,</span> [<span class='function'>fizzData</span>]
<span class='function'>loopStart:</span>
	<span class='keyword'>inc</span>	rcx
	<span class='keyword'>cmp</span>	rcx<span class='keyword'>,</span> <span class='number'>50</span>		<span class='comment'>; Define loop end</span>
	<span class='keyword'>jg</span>	<span class='function'>exit</span>

	<span class='keyword'>call</span>	<span class='function'>printNewLine</span>
	<span class='keyword'>push</span>	rdi
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rcx
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> <span class='number'>10</span>
	<span class='keyword'>call</span>	<span class='function'>printNumber</span>

	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='function'>strSeparator</span>
	<span class='keyword'>call</span>	<span class='function'>print</span>

	<span class='keyword'>pop</span>	rdi

	<span class='keyword'>xor</span>	r8<span class='keyword'>,</span> r8
<span class='function'>arrayLoopStart:</span>
	<span class='keyword'>cmp</span>	r8<span class='keyword'>,</span> <span class='number'>16</span>		<span class='comment'>; 8 * fizzData array length</span>
	<span class='keyword'>jge</span>	<span class='function'>loopStart</span>
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> [rdi+r8]
	<span class='keyword'>add</span>	r8<span class='keyword'>,</span> <span class='number'>8</span>
	<span class='keyword'>xor</span>	rbx<span class='keyword'>,</span> rbx
	<span class='keyword'>mov</span>	bl<span class='keyword'>,</span> [rsi]
	<span class='keyword'>cmp</span>	rcx<span class='keyword'>,</span> rbx
	<span class='keyword'>je</span>	<span class='function'>printString</span>
	<span class='keyword'>jl</span>	<span class='function'>arrayLoopStart</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> rcx	<span class='comment'>; Put counter in quotient</span>
	<span class='keyword'>xor</span>	rdx<span class='keyword'>,</span> rdx	<span class='comment'>; Zero remainder</span>
	<span class='keyword'>div</span>	rbx
	<span class='keyword'>cmp</span>	rdx<span class='keyword'>,</span> <span class='number'>0</span>
	<span class='keyword'>jnz</span>	<span class='function'>arrayLoopStart</span>
<span class='function'>printString:</span>
	<span class='keyword'>push</span>	rdi
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rsi
	<span class='keyword'>add</span>	rdi<span class='keyword'>,</span> <span class='number'>2</span>
	<span class='keyword'>call</span>	<span class='function'>print</span>
	<span class='keyword'>pop</span>	rdi
	<span class='keyword'>jmp</span>	<span class='function'>arrayLoopStart</span>

<span class='function'>exit:</span>
	<span class='keyword'>call</span>	<span class='function'>printNewLine</span>
	<span class='keyword'>call</span>	<span class='function'>quit</span>


	<span class='keyword'>section .rodata</span>
<span class='function'>intro_1:</span>	<span class='keyword'>db</span>	<span class='string'>"Fizz Buzz"</span><span class='keyword'>,</span> <span class='number'>0</span>
<span class='function'>intro_2:</span>	<span class='keyword'>db</span>	<span class='string'>"---------"</span><span class='keyword'>,</span> <span class='number'>0</span>
<span class='function'>strSeparator:</span>	<span class='keyword'>db</span>	<span class='string'>": "</span><span class='keyword'>,</span> <span class='number'>0</span>
<span class='function'>strFizz:</span>	<span class='keyword'>db</span>	<span class='number'>3</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='string'>"Fizz"</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='number'>0</span>
<span class='function'>strBazz:</span>	<span class='keyword'>db</span>	<span class='number'>4</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='string'>"Bazz"</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='number'>0</span>
<span class='function'>strBuzz:</span>	<span class='keyword'>db</span>	<span class='number'>5</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='string'>"Buzz"</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='number'>0</span>
<span class='function'>strFozz:</span>	<span class='keyword'>db</span>	<span class='number'>7</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span> <span class='string'>"Fozz"</span><span class='keyword'>,</span> <span class='number'>0</span><span class='keyword'>,</span><span class='number'> 0</span>
<span class='comment'>;fizzData:	dq	strFizz, strBazz, strBuzz, strFozz
<span class='function'>fizzData:</span>	<span class='keyword'>dq</span>	<span class='function'>strFizz</span><span class='keyword'>,</span> <span class='function'>strBuzz</span>

			`
export const fizzBuzzExe = `
<span class='bold pink'>phil</span>@<span class='bold number'>os</span>:<span class='bold string'>~/My Documents/Assembly/FizzBuzz</span>
<span class='bold string'>$</span> ./fizzBuzz 

Fizz Buzz
------------------
1:
2:
3: Fizz
4:
5: Buzz
6: Fizz
7:
8:
9: Fizz
10: Buzz
11:
12: Fizz
13:
14:
15: FizzBuzz
16:
17:
18: Fizz
19:
20: Buzz
21: Fizz
22:
23:
24: Fizz
25: Buzz
26:
27: Fizz
28:
29:
30: FizzBuzz
31:
32:
33: Fizz
34:
35: Buzz
36: Fizz
37:
38:
39: Fizz
40: Buzz
41:
42: Fizz
43:
44:
45: FizzBuzz
46:
47:
48: Fizz
49:
50: Buzz
			`
export const echoAsm = `
<span class='comment'>; echo %s</span>

<span class='comment'>; To assemble and run:</span>
<span class='comment'>;	nasm -felf64 echo.asm && ld echo.o -o echo && ./echo "my message"</span>

%include <span class='string'>'../common/functions.asm'</span>

	<span class='keyword'>section .text</span>
	<span class='keyword'>global</span> <span class='function'>_start</span>
<span class='function'>_start:</span>
	<span class='keyword'>mov</span>	eax<span class='keyword'>,</span> [rsp]
	<span class='keyword'>cmp</span>	eax<span class='keyword'>,</span> <span class='number'>2</span>
	<span class='keyword'>jl</span>	<span class='function'>exit</span>

	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> [rsp+<span class='number'>16</span>]
	<span class='keyword'>call</span>	<span class='function'>printLine</span>
<span class='function'>exit:</span>
	<span class='keyword'>call</span>	<span class='function'>quit</span>
			`
export const echoExe = `
<span class='bold pink'>phil</span>@<span class='bold number'>os</span>:<span class='bold string'>~/My Documents/Assembly/Echo</span>
<span class='bold string'>$</span> ./echo "Hello, world!" 

Hello, world!
			`
export const functionsAsm = `
<span class='comment'>;--------------------------------------</span>
<span class='comment'>; int maths_power(int base, int power)</span>
<span class='function'>maths_power:</span>
	<span class='keyword'>push</span>	rbx
	<span class='keyword'>push</span>	rcx

	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> rdi
	<span class='keyword'>cmp</span>	rdi<span class='keyword'>,</span> <span class='number'>0</span>			<span class='comment'>; if the base is 0, return 0</span>
	<span class='keyword'>jz</span>	<span class='function'>return__maths_power</span>
	<span class='keyword'>cmp</span>	rsi<span class='keyword'>,</span> <span class='number'>1</span>			<span class='comment'>; if the power is 1, return the base</span>
	<span class='keyword'>je</span>	<span class='function'>return__maths_power</span>
	<span class='keyword'>cmp</span>	rsi<span class='keyword'>,</span> <span class='number'>0</span>			<span class='comment'>; if the power is 0, return 1</span>
	<span class='keyword'>jz</span>	<span class='function'>return_1__maths_power</span>

	<span class='keyword'>mov</span>	rcx<span class='keyword'>,</span> rsi
	<span class='keyword'>mov</span>	rbx<span class='keyword'>,</span> rax
<span class='function'>loopStart__maths_power:</span>
	<span class='keyword'>cmp</span>	rcx<span class='keyword'>,</span> <span class='number'>1</span>
	<span class='keyword'>jz</span>	<span class='function'>return__maths_power</span>
	<span class='keyword'>mul</span>	rbx
	<span class='keyword'>dec</span>	rcx
	<span class='keyword'>jmp</span>	<span class='function'>loopStart__maths_power</span>

<span class='function'>return_1__maths_power:</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> <span class='number'>1</span>

<span class='function'>return__maths_power:</span>
	<span class='keyword'>pop</span>	rcx
	<span class='keyword'>pop</span>	rbx
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; int string_getLength(String message)</span>
<span class='function'>string_getLength:</span>
	<span class='keyword'>push</span>	rcx
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> rdi
	<span class='keyword'>mov</span>	rcx<span class='keyword'>,</span> rax

<span class='function'>incrementStringLength__string_getLength:</span>
	<span class='keyword'>cmp</span>	<span class='string'>byte</span> [rax]<span class='keyword'>,</span> <span class='number'>0</span>	<span class='comment'>; if next byte is not 0, increment RAX</span>
	<span class='keyword'>jz</span>	<span class='function'>finishedIncrementing__string_getLength</span>
	<span class='keyword'>inc</span>	rax
	<span class='keyword'>jmp</span>	<span class='function'>incrementStringLength__string_getLength</span>

<span class='function'>finishedIncrementing__string_getLength:</span>
	<span class='keyword'>sub</span>	rax<span class='keyword'>,</span> rcx	<span class='comment'>; find difference between RAX and RCX</span>
				<span class='comment'>; this is the string length</span>
	<span class='keyword'>pop</span>	rcx
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; void print(String message)</span>
<span class='function'>print:</span>
	<span class='keyword'>push</span>	rax
	<span class='keyword'>push</span>	rsi

	<span class='keyword'>call</span>	<span class='function'>string_getLength</span>
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> rax	<span class='comment'>; string length in bytes</span>
	<span class='keyword'>call</span>	<span class='function'>__doPrint</span>

	<span class='keyword'>pop</span>	rsi
	<span class='keyword'>pop</span>	rax
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; void printCharacter(Char character)</span>
<span class='function'>printCharacter:</span>
	<span class='keyword'>push</span>	rsi

	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> <span class='number'>1</span>
	<span class='keyword'>call</span>	<span class='function'>__doPrint</span>

	<span class='keyword'>pop</span>	rsi
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; void printLine(String message)</span>
<span class='function'>printLine:</span>
	<span class='keyword'>call</span>	<span class='function'>print</span>
	<span class='keyword'>call</span>	<span class='function'>printNewLine</span>
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; void printNewLine()</span>
<span class='function'>printNewLine:</span>
	<span class='keyword'>push</span>	rdi
	<span class='keyword'>push</span>	<span class='number'>10</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rsp
	<span class='keyword'>call</span>	<span class='function'>printCharacter</span>
	<span class='keyword'>pop</span>	rdi
	<span class='keyword'>pop</span>	rdi
	<span class='keyword'>ret</span>

<span class='comment'>;--------------------------------------</span>
<span class='comment'>; void printNumber(int number, int base)</span>
<span class='function'>printNumber:</span>
	<span class='keyword'>push</span>	rdi
	<span class='keyword'>push</span>	rax
	<span class='keyword'>push</span>	rbx
	<span class='keyword'>push</span>	rcx

	<span class='keyword'>xor</span>	rcx<span class='keyword'>,</span> rcx
<span class='function'>start__printNumber:</span>
	<span class='keyword'>call</span>	<span class='function'>getIntegerAsCharacter</span>	<span class='comment'>; RAX is now character code</span>
					<span class='comment'>; RBX is new number (old number / base)</span>
	<span class='keyword'>push</span>	rax

	<span class='keyword'>cmp</span>	rbx<span class='keyword'>,</span> <span class='number'>0</span>			<span class='comment'>; if RBX is 0, we've converted every digit</span>
	<span class='keyword'>jz</span>	<span class='function'>doPrint__printNumber</span>

	<span class='keyword'>inc</span>	rcx
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rbx
	<span class='keyword'>jmp</span>	<span class='function'>start__printNumber</span>

<span class='function'>doPrint__printNumber:</span>			<span class='comment'>; print characters in reverse order</span>
	<span class='keyword'>cmp</span>	rcx<span class='keyword'>,</span> <span class='number'>0</span>
	<span class='keyword'>jl</span>	<span class='function'>exit__printNumber</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> rsp
	<span class='keyword'>dec</span>	rcx
	<span class='keyword'>call</span>	<span class='function'>printCharacter</span>
	<span class='keyword'>pop</span>	rax
	<span class='keyword'>jmp</span>	<span class='function'>doPrint__printNumber</span>

<span class='function'>exit__printNumber:</span>
	<span class='keyword'>pop</span>	rcx
	<span class='keyword'>pop</span>	rbx
	<span class='keyword'>pop</span>	rax
	<span class='keyword'>pop</span>	rdi
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; String, int getIntegerAsCharacter(int number, int base)</span>
<span class='function'>getIntegerAsCharacter:</span>
	<span class='keyword'>push</span>	rdx

	<span class='keyword'>cmp</span>	rdi<span class='keyword'>,</span> rsi	<span class='comment'>; if integer < base, return integer</span>
	<span class='keyword'>jge</span>	<span class='function'>doDivision__getIntegerAsCharacter</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> rdi
	<span class='keyword'>xor</span>	rbx<span class='keyword'>,</span> rbx
	<span class='keyword'>jmp</span>	<span class='function'>returnInteger__getIntegerAsCharacter</span>

<span class='function'>doDivision__getIntegerAsCharacter:</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> rdi	<span class='comment'>; put parameter 1 (dividend) in rax</span>
	<span class='keyword'>mov</span>	rbx<span class='keyword'>,</span> rsi	<span class='comment'>; put parameter 2 (base) in rbx</span>
	<span class='keyword'>xor</span>	rdx<span class='keyword'>,</span> rdx	<span class='comment'>; zero our remainder register</span>
	<span class='keyword'>div</span>	rbx		<span class='comment'>; RAX is the answer; RDX is the remainder</span>
	<span class='keyword'>mov</span>	rbx<span class='keyword'>,</span> rax	<span class='comment'>; put new integer (the result) in RBX</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> rdx	<span class='comment'>; put remainder in RAX</span>

<span class='function'>returnInteger__getIntegerAsCharacter:</span>
	<span class='keyword'>add</span>	rax<span class='keyword'>,</span> <span class='number'>48</span>		<span class='comment'>; Turn number to ASCII character</span>

	<span class='keyword'>pop</span>	rdx
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; int readNumber(Address stringLocation)</span>
<span class='function'>readNumber:</span>
	<span class='keyword'>push</span>	rsi
	<span class='keyword'>push</span>	rbx
	<span class='keyword'>push</span>	rcx
	<span class='keyword'>push</span>	rdx

	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> rdi
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='number'>10</span>
	<span class='keyword'>call</span>	<span class='function'>read</span>
	<span class='keyword'>dec</span>	rax
	<span class='keyword'>mov</span>	rbx<span class='keyword'>,</span> rax		<span class='comment'>; length of string entered</span>


	<span class='comment'>; After reading in string, loop through input</span>
	<span class='comment'>; subtract 48 from each numeric value (48 <= value <= 57)</span>
	<span class='comment'>; and multiply it by the base of the digit</span>
	<span class='keyword'>xor</span>	rax<span class='keyword'>,</span> rax
	<span class='keyword'>mov</span>	rcx<span class='keyword'>,</span> rsi
<span class='function'>loopStart__readNumber:</span>
	<span class='comment'>; <span class='bold string'>TODO: add support receiving a minus character first (chr(45))</span></span>
	<span class='keyword'>cmp</span>	<span class='string'>byte</span> [rcx]<span class='keyword'>,</span> <span class='number'>48</span>		<span class='comment'>; if next byte is < chr(48)</span>
	<span class='keyword'>jl</span>	<span class='function'>return__readNumber</span>	<span class='comment'>; then exit</span>
	<span class='keyword'>cmp</span>	<span class='string'>byte</span> [rcx]<span class='keyword'>,</span> <span class='number'>57</span>		<span class='comment'>; if next byte is > chr(57)</span>
	<span class='keyword'>jg</span>	<span class='function'>return__readNumber</span>	<span class='comment'>; remove the character</span>
	<span class='keyword'>movzx</span>	rdx<span class='keyword'>,</span> <span class='string'>byte</span> [rcx]
	<span class='keyword'>sub</span>	rdx<span class='keyword'>,</span> <span class='number'>48</span>
	<span class='keyword'>push</span>	rax
	<span class='keyword'>push</span>	rdx
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='number'>10</span>
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> rbx
	<span class='keyword'>call</span>	<span class='function'>maths_power</span>
	<span class='keyword'>pop</span>	rdx
	<span class='keyword'>mul</span>	rdx
	<span class='keyword'>mov</span>	rdx<span class='keyword'>,</span> rax
	<span class='keyword'>pop</span>	rax
	<span class='keyword'>add</span>	rax<span class='keyword'>,</span> rdx
	<span class='keyword'>dec</span>	rbx
	<span class='keyword'>inc</span>	rcx
	<span class='keyword'>jmp</span>	<span class='function'>loopStart__readNumber</span>

<span class='function'>return__readNumber:</span>
	<span class='keyword'>pop</span>	rdx
	<span class='keyword'>pop</span>	rcx
	<span class='keyword'>pop</span>	rbx
	<span class='keyword'>pop</span>	rsi
	<span class='keyword'>ret</span>

<span class='comment'>;--------------------------------------</span>
<span class='comment'>; int read(int stringLength, Address stringLocation)</span>
<span class='comment'>; returns length of string entered in RAX</span>
<span class='function'>read:</span>
	<span class='keyword'>push</span>	r8
	<span class='keyword'>push</span>	r9
	<span class='keyword'>push</span>	rbx
	<span class='keyword'>push</span>	rcx
	<span class='keyword'>push</span>	rdx
	<span class='keyword'>push</span>	rdi
	<span class='keyword'>xor</span>	rbx<span class='keyword'>,</span> rbx
	<span class='keyword'>push</span>	rbx			<span class='comment'>; extra push to ensure there's space on the stack for our input</span>

	<span class='keyword'>mov</span>	rbx<span class='keyword'>,</span> rsi		<span class='comment'>; store original string buffer</span>
	<span class='keyword'>mov</span>	r8<span class='keyword'>,</span> rdi			<span class='comment'>; store original string length</span>
	<span class='keyword'>xor</span>	r9<span class='keyword'>,</span> r9			<span class='comment'>; count length of entered string</span>
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> rsp		<span class='comment'>; point input buffer at the top of the stack</span>
	<span class='keyword'>xor</span>	rdi<span class='keyword'>,</span> rdi		<span class='comment'>; file handle 0 is stdin</span>

<span class='function'>loopStart__readImproved:</span>
	<span class='keyword'>xor</span>	rax<span class='keyword'>,</span> rax		<span class='comment'>; system call for read</span>
	<span class='keyword'>mov</span>	rdx<span class='keyword'>,</span> <span class='number'>1</span>			<span class='comment'>; length of string - we're reading one character at a time</span>
	<span class='keyword'>syscall</span>				<span class='comment'>; invoke OS to do the read</span>

	<span class='keyword'>cmp</span>	<span class='string'>byte</span> [rsi]<span class='keyword'>,</span> <span class='number'>0</span>		<span class='comment'>; if next byte is null, return</span>
	<span class='keyword'>jz</span>	<span class='function'>return__readImproved</span>
	<span class='keyword'>cmp</span>	<span class='string'>byte</span> [rsi]<span class='keyword'>,</span> <span class='number'>10</span>		<span class='comment'>; if next byte is line-break, return</span>
	<span class='keyword'>je</span>	<span class='function'>return__readImproved</span>
	<span class='keyword'>cmp</span>	r8<span class='keyword'>,</span> <span class='number'>0</span>			<span class='comment'>; if we're under the allowed input limit, continue looping</span>
	<span class='keyword'>jz</span>	<span class='function'>loopStart__readImproved</span>
	<span class='keyword'>movzx</span>	rdx<span class='keyword'>,</span> <span class='string'>byte</span> [rsi]
	<span class='keyword'>mov</span>	[rbx+r9]<span class='keyword'>,</span> rdx
	<span class='keyword'>inc</span>	r9
	<span class='keyword'>dec</span>	r8
	<span class='keyword'>jmp</span>	<span class='function'>loopStart__readImproved</span>

<span class='function'>return__readImproved:</span>
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> rbx
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> r9
	<span class='keyword'>pop</span>	rbx
	<span class='keyword'>pop</span>	rdi
	<span class='keyword'>pop</span>	rdx
	<span class='keyword'>pop</span>	rcx
	<span class='keyword'>pop</span>	rbx
	<span class='keyword'>pop</span>	r9
	<span class='keyword'>pop</span>	r8
	<span class='keyword'>ret</span>


<span class='comment'>;--------------------------------------</span>
<span class='comment'>; void quit()</span>
<span class='function'>quit:</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> <span class='number'>60</span>		<span class='comment'>; system call for exit</span>
	<span class='keyword'>xor</span>	rdi<span class='keyword'>,</span> rdi	<span class='comment'>; exit code 0</span>
	<span class='keyword'>syscall</span>			<span class='comment'>; invoke OS to exit</span>


<span class='comment'>; Internal functions</span>
<span class='function'>__doPrint:</span>
	<span class='keyword'>push</span>	rax
	<span class='keyword'>push</span>	rcx
	<span class='keyword'>push</span>	rdi
	<span class='keyword'>push</span>	rdx
	<span class='keyword'>push</span>	rsi

	<span class='keyword'>mov</span>	rdx<span class='keyword'>,</span> rsi	<span class='comment'>; length of string</span>
	<span class='keyword'>mov</span>	rsi<span class='keyword'>,</span> rdi	<span class='comment'>; address of string</span>
	<span class='keyword'>mov</span>	rax<span class='keyword'>,</span> <span class='number'>1</span>		<span class='comment'>; system call for write</span>
	<span class='keyword'>mov</span>	rdi<span class='keyword'>,</span> <span class='number'>1</span>		<span class='comment'>; file handle 1 is stdout</span>
	<span class='keyword'>syscall</span>			<span class='comment'>; invoke OS to do the write</span>

	<span class='keyword'>pop</span>	rsi
	<span class='keyword'>pop</span>	rdx
	<span class='keyword'>pop</span>	rdi
	<span class='keyword'>pop</span>	rcx
	<span class='keyword'>pop</span>	rax
	<span class='keyword'>ret</span>

	`
export const aboutMeTxt = `Y'all right?

This is obviously a work-in-progress so expect to see absolutely no change for the next six or seven years (at which point I'll replace it with something completely different.)

Phil`
export const todoMd = `<h3>To-do</h3>
<ul>
	<li>Add Start menu</li>
	<li>More theming</li>
	<li>Add boot process (or at least log-in process)</li>
	<li>Add more applications (web-browser, games etc.)</li>
	<li>Stagger the position of apps when they open</li>
	<li>Check whether I'm in violation of copyright by using WinXp's Bliss.jpg background...</li>
</ul>`
export const cvOdt = `
<div class='flex border-bottom p-b-20'>
  <div class='col-7'>
    <h1 class='m-0'>Phil Armstead</h1>
    <p class='m-0'><em>Senior Frontend Engineer</em></p>
  </div>
  <div class='col-5'>
    <p>
      Manchester, UK<br>
      <img src='/assets/icons/linkedIn.svg' alt='LinkedIn' style='max-width: 16px; vertical-align: text-top; margin-right: 1px;'>
      <a href='https://www.linkedin.com/in/philarmstead/' target='_blank' aria-label='Visit my LinkedIn profile'>PhilArmstead</a>
    </p>
  </div>
</div>
<div class='flex border-bottom m-t-20 p-b-20'>
  <div class='col-3'>
    <h2>Professional summary</h2>
  </div>
  <div class='col-9'>
    <p>
      Award-winning full-stack web developer with over 12 years' agency and in-house experience, having worked for
      some of the biggest brands and musical artists in the world.<br>
      <br>
      Meticulous and hard-working, I strive for creative and efficient solutions in all endeavours, whether it's writing
      software, creating Node apps or turning a designer's mock-up in to a pixel-perfect page.
    </p>
  </div>
</div>
<div class='flex border-bottom m-t-20 p-b-20'>
  <div class='col-3'>
    <h2>Key skills</h2>
  </div>
  <div class='col-9'>
    <p>
      HTML and JavaScript. I still consider those to be key skills.
      This section used to be a dispassionate list of every technology I'd ever used but, at this age,
      it became excessive. At heart, I'm a front-end developer. I love making websites. I made this in Vue and TypeScript,
      but it could just as easily have been done with Svelte, Web Components or vanilla JS.
    </p>
  </div>
</div>
<div class='flex border-bottom m-t-20 p-b-20'>
  <div class='col-2'>
    <h2>Just for fun</h2>
  </div>
	<div class='col-10'>
		<div class='flex'>
		  <div class='col-5' style='margin-bottom: 0'>
		    <ul>
		      <li>80% Linux, Steam% Windows
		      <li>Bash, Python automator
		    </ul>
		  </div>
		  <div class='col-5'>
		    <ul>
		      <li>C, C++, Assembly tinkerer
		      <li>Raspberry Pi experimenter
		    </ul>
		  </div>		
		</div>		
		<div class='flex'>
			<div class='col-10'>
				<ul>
	        <li>Most recent interest: disassembling video games and writing old-school trainers for them (sans the music)
				</ul>
			</div>
		</div>
	</div>
</div>
<div class='flex m-t-20'>
  <div class='col-2'>
    <h2>Interests</h2>
  </div>
  <div class='col-9'>
    <p>
      I like to spend my free time playing video games, reading books or keeping fit. My favourite game is Final Fantasy VII,
      my favourite book is To Kill a Mockingbird and my fastest 5k is 21:22. In the future I'd like to complete some sort of
      outdoor expedition (Tough Mudder, Go Ape etc.) and implement my own artificial neural network.
    </p>
  </div>
</div>
		`
