import { FileType } from '../enums/files'
import { MacIconDirectory } from '../assets/icons/macos'
import { WindowsIconDirectory } from '../assets/icons/windows'

export const ProcessIcon: Record<string, Record<FileType, string>> = {
	win: {
		[FileType.DIRECTORY]: WindowsIconDirectory,
	},
	mac: {
		[FileType.DIRECTORY]: MacIconDirectory,
	},
}
