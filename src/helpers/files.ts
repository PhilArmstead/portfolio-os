import type { Component } from 'vue'

import { CodeViewer, CommandPrompt, FileExplorer, ImageViewer, ModalAlert, Notepad } from '../components/processes'
import { createProcess } from '../store/processes'
import type { File } from '../../types'
import { FileType } from '../enums/files'
import { getIconFromFileType } from './icons'

export const openFile = (file: File): void => {
	// Deep copy file, so we can modify it without changing the source
	const fileToOpen: File = Object.assign({}, { ...file, data: { ...file.data }, meta: { ...file.meta } })
	const title = `${fileToOpen.meta.title || fileToOpen.name}${fileToOpen.meta.readonly ? ' (readonly)' : ''}`
	let component: Component | undefined

	switch (file.type) {
		case FileType.IMAGE:
			// TODO: give ImageViewer its own icon
			component = ImageViewer
			fileToOpen.meta.title = `Image viewer: ${title}`
			break

		case FileType.RICH_TEXT:
		case FileType.TEXT:
			component = Notepad
			fileToOpen.meta.defaultSize ??= { height: 330, width: 540 }
			fileToOpen.meta.title = `Wrotepad: ${title}`
			break

		case FileType.CODE:
			component = CodeViewer
			fileToOpen.meta.defaultSize ??= { height: 560, width: 660 }
			fileToOpen.meta.title = `Codepad: ${title}`
			break

		case FileType.SHELL:
			// TODO: have this component open to an empty terminal,
			//  type out the command,
			//  wait and then dump the content ^_^
			component = CommandPrompt
			fileToOpen.meta.defaultSize ??= { height: 280, width: 500 }
			fileToOpen.meta.title = `Shell: ${title}`
			break

		case FileType.DIRECTORY:
			component = FileExplorer
			fileToOpen.meta.defaultSize ??= { height: 260, width: 500 }
			break

		// TODO: customise dismiss CTA(s?)
		case FileType.ALERT:
			component = ModalAlert
			fileToOpen.meta.cannotMaximise ??= true
			fileToOpen.meta.cannotMinimise ??= true
			break
	}

	if (fileToOpen && component) {
		if (!fileToOpen.meta.title) {
			fileToOpen.meta.title = title
		}

		if (!fileToOpen.icon) {
			fileToOpen.icon = getIconFromFileType(fileToOpen.type)
		}

		createProcess(fileToOpen, component)
	}
}
