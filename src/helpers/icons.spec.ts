import { describe, expect, it, vi } from 'vitest'

import { FileType } from '../enums/files'
import { getIconFromFileType } from './icons'

vi.mock('../data/themes', () => ({
	ProcessIcon: {
		'my-os': {
			[FileType.DIRECTORY]: 'os-specific-directory',
		},
	},
}))
vi.mock('../enums/icons', () => ({
	ProcessIcons: {
		[FileType.DIRECTORY]: 'generic-directory',
		[FileType.RICH_TEXT]: 'generic-rich-text',
	},
}))
vi.mock('../store/client', () => ({ operatingSystem: 'my-os' }))

describe('getIconFromFileType', () => {
	describe('When file type has OS-specific icon', () => {
		it('Should return OS icon', () => {
			expect(getIconFromFileType(FileType.DIRECTORY)).toBe('os-specific-directory')
		})
	})

	describe('When file type does not have OS-specific icon', () => {
		it('Should return non-OS icon', () => {
			expect(getIconFromFileType(FileType.RICH_TEXT)).toBe('generic-rich-text')
		})
	})
})
