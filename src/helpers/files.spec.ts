import { beforeAll, describe, expect, it, vi } from 'vitest'
import type { Component } from 'vue'

import { CodeViewer, CommandPrompt, FileExplorer, ImageViewer, ModalAlert, Notepad } from '../components/processes'
import type { File } from '../../types'
import { FileType } from '../enums/files'
import { openFile } from './files'

const mockOpenProcess = vi.fn()

vi.mock('../store/processes', () => ({
	createProcess: (file: File, component: Component): void => mockOpenProcess(file, component),
}))

describe('openFile', () => {

	const boringImage: File = {
		data: {},
		meta: {},
		name: 'Basic image',
		type: FileType.IMAGE,
	}
	const boringModifiedImage: File = {
		...boringImage,
		meta: { title: `Image viewer: ${boringImage.name}` },
	}
	const specialImage: File = {
		data: { content: '/path/to/image.jpg' },
		icon: '/path/to/icon.jpg',
		meta: { readonly: true, title: 'My special title' },
		name: 'Custom image',
		type: FileType.IMAGE,
	}
	const specialModifiedImage: File = {
		...specialImage,
		meta: {
			...specialImage.meta,
			title: `Image viewer: ${specialImage.meta.title} (readonly)`,
		},
	}
	const boringCode: File = {
		data: {},
		meta: {},
		name: 'Basic code',
		type: FileType.CODE,
	}
	const boringModifiedCode: File = {
		...boringCode,
		icon: '/assets/icons/code.svg',
		meta: {
			defaultSize: { height: 560, width: 660 },
			title: `Codepad: ${boringCode.name}`,
		},
	}
	const specialCode: File = {
		data: { content: '<!-- My code -->' },
		icon: '/path/to/icon.jpg',
		meta: { defaultSize: { height: 999, width: 888 }, readonly: true, title: 'Super code' },
		name: 'Custom code',
		type: FileType.CODE,
	}
	const specialModifiedCode: File = {
		...specialCode,
		meta: {
			...specialCode.meta,
			defaultSize: { height: 999, width: 888 },
			title: `Codepad: ${specialCode.meta.title} (readonly)`,
		},
	}
	const boringShell: File = {
		data: {},
		meta: {},
		name: 'Basic code',
		type: FileType.SHELL,
	}
	const boringModifiedShell: File = {
		...boringShell,
		icon: '/assets/icons/shell.svg',
		meta: {
			defaultSize: { height: 280, width: 500 },
			title: `Shell: ${boringShell.name}`,
		},
	}
	const specialShell: File = {
		data: { content: './print "Hello, world!"' },
		icon: '/path/to/icon.jpg',
		meta: { defaultSize: { height: 999, width: 888 }, readonly: true, title: 'Super prompt' },
		name: 'Custom prompt',
		type: FileType.SHELL,
	}
	const specialModifiedShell: File = {
		...specialShell,
		meta: {
			...specialShell.meta,
			defaultSize: { height: 999, width: 888 },
			title: `Shell: ${specialShell.meta.title} (readonly)`,
		},
	}
	const boringDirectory: File = {
		data: {},
		meta: {},
		name: 'Basic code',
		type: FileType.DIRECTORY,
	}
	const boringModifiedDirectory: File = {
		...boringDirectory,
		icon: '/assets/icons/windows/directory.svg',
		meta: {
			defaultSize: { height: 260, width: 500 },
			title: boringDirectory.name,
		},
	}
	const specialDirectory: File = {
		data: { files: [] },
		icon: '/path/to/icon.jpg',
		meta: { defaultSize: { height: 999, width: 888 }, title: 'My Documents' },
		name: '/usr/phil/documents',
		type: FileType.DIRECTORY,
	}
	const specialModifiedDirectory: File = {
		...specialDirectory,
		meta: {
			...specialDirectory.meta,
			defaultSize: { height: 999, width: 888 },
			title: specialDirectory.meta.title,
		},
	}
	const boringAlert: File = {
		data: {},
		meta: {},
		name: 'Basic alert',
		type: FileType.ALERT,
	}
	const boringModifiedAlert: File = {
		...boringAlert,
		icon: '/assets/icons/alert.svg',
		meta: {
			cannotMaximise: true,
			cannotMinimise: true,
			title: boringAlert.name,
		},
	}
	const specialAlert: File = {
		data: { files: [] },
		icon: '/path/to/icon.jpg',
		meta: {
			cannotMaximise: false,
			cannotMinimise: false,
			defaultSize: { height: 999, width: 888 },
			title: 'My Documents',
		},
		name: 'Error!',
		type: FileType.ALERT,
	}
	const specialModifiedAlert: File = {
		...specialAlert,
		meta: {
			...specialAlert.meta,
			title: specialAlert.meta.title,
		},
	}
	const boringText: File = {
		data: {},
		meta: {},
		name: 'Basic text',
		type: FileType.TEXT,
	}
	const boringModifiedText: File = {
		...boringText,
		icon: '/assets/icons/notepad.svg',
		meta: { defaultSize: { height: 330, width: 540 }, title: `Wrotepad: ${boringText.name}` },
	}
	const specialText: File = {
		data: { files: [] },
		icon: '/path/to/icon.jpg',
		meta: {
			defaultSize: { height: 999, width: 888 },
			title: 'My Secret Document',
		},
		name: 'Error!',
		type: FileType.TEXT,
	}
	const specialModifiedText: File = {
		...specialText,
		meta: {
			...specialText.meta,
			title: `Wrotepad: ${specialText.meta.title}`,
		},
	}
	const boringRichText: File = {
		data: {},
		meta: {},
		name: 'Basic text',
		type: FileType.RICH_TEXT,
	}
	const boringModifiedRichText: File = {
		...boringRichText,
		icon: '/assets/icons/notepad.svg',
		meta: { defaultSize: { height: 330, width: 540 }, title: `Wrotepad: ${boringRichText.name}` },
	}
	const specialRichText: File = {
		data: { files: [] },
		icon: '/path/to/icon.jpg',
		meta: {
			defaultSize: { height: 999, width: 888 },
			title: 'My Secret Document',
		},
		name: 'Error!',
		type: FileType.RICH_TEXT,
	}
	const specialModifiedRichText: File = {
		...specialRichText,
		meta: {
			...specialRichText.meta,
			title: `Wrotepad: ${specialRichText.meta.title}`,
		},
	}
	describe.each([
		['Image', [
			['no optional', boringImage, boringModifiedImage, false],
			['optional', specialImage, specialModifiedImage, false],
		], ImageViewer],
		['Code', [
			['no optional', boringCode, boringModifiedCode, false],
			['optional', specialCode, specialModifiedCode, false],
		], CodeViewer],
		['Shell', [
			['no optional', boringShell, boringModifiedShell, false],
			['optional', specialShell, specialModifiedShell, false],
		], CommandPrompt],
		['Directory', [
			['no optional', boringDirectory, boringModifiedDirectory, false],
			['optional', specialDirectory, specialModifiedDirectory, true],
		], FileExplorer],
		['Alert', [
			['no optional', boringAlert, boringModifiedAlert, false],
			['optional', specialAlert, specialModifiedAlert, true],
		], ModalAlert],
		['Text', [
			['no optional', boringText, boringModifiedText, false],
			['optional', specialText, specialModifiedText, false],
		], Notepad],
		['RichText', [
			['no optional', boringRichText, boringModifiedRichText, false],
			['optional', specialRichText, specialModifiedRichText, false],
		], Notepad],
	])('When calling with %s file', (_, tests, component) => {
		describe.each(tests)('And file contains %s props', (_, file, modifiedFile, willModify) => {
			beforeAll(() => {
				openFile(file as File)
			})

			it('Should call `files` with modified file object', () => {
				expect(mockOpenProcess).toHaveBeenCalledWith(modifiedFile, component)
			})

			if (!willModify) {
				it('Should not modify file argument', () => {
					expect(file).not.toEqual(modifiedFile)
				})
			}
		})
	})
})
