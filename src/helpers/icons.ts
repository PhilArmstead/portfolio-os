import type { FileType } from '../enums/files'
import { operatingSystem } from '../store/client'
import { ProcessIcon } from '../data/themes'
import { ProcessIcons } from '../enums/icons'

export const getIconFromFileType = (fileType: FileType): string =>
	ProcessIcon[operatingSystem][fileType] || ProcessIcons[fileType]
