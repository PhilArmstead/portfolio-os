# Actual TODO

- Explorer should keep a history and have a UI
- Add Start menu
- More theme options
- Add boot process (or at least log-in process)
- Add more applications (web-browser, games etc.)
- Stagger the position of apps when they open
- Use WebP image formats
- Routing to land on the page with an app open
- Static rendering with client-side hydration
- Arrow-keys to traverse file-list
